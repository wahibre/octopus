/**
 ** This file is part of the Octopus project.
 ** Copyright 2023 wahibre <wahibre@gmx.com>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#ifndef HISTORIAMODEL_H
#define HISTORIAMODEL_H

#include <QAbstractItemModel>
#include <QDomDocument>

#include "prikaz.h"

#define HIST_ELEM_HISTORIA "historia"
#define HIST_ELEM_PRIKAZ "prikaz"
#define HIST_ELEM_PROGRAM "program"
#define HIST_ELEM_PARAMETER "parameter"
#define HIST_ELEM_ENVIRONMENT "environment"
#define HIST_ATTR_NAZOV "nazov"
#define HIST_ELEM_WORKDIR "workdir"

class HistoriaModel : public QAbstractItemModel
{
    Q_OBJECT
private:
    QDomDocument DomHistory;
    QString dataLocation;
    QDomNodeList prikazy;
    QMap<int, QString> HistHeadersNames;
    QString fullTextSearch;
    Qt::CaseSensitivity fullTextSearchSensitivity;
public:
    HistoriaModel(QObject *parent = nullptr);

    bool loadData();
    void saveData();
    QString getFullTextSearch() const;
    void setFullTextSearch(const QString &value, Qt::CaseSensitivity sensitivity);
    QStringList vsetkyParametreHistorie();
    Prikaz dajPrikaz(int row);
    int pocet();
    QString dajProgram(const QModelIndex &idx) const;
    QString dajParams(const QModelIndex &idx) const;
    bool existujeTextVProgram(const QModelIndex &idx, QString text, Qt::CaseSensitivity caseSensitivity) const;
    bool existujeTextVParams(const QModelIndex &idx, QString text, Qt::CaseSensitivity caseSensitivity) const;
    bool existujeTextVRiadku(const QModelIndex &idx, QString text, Qt::CaseSensitivity caseSensitivity) const;
    QDomDocument vytvorDOM(QModelIndexList vybrane, int &pocetExportovanych);
    void zmazRiadky(QModelIndexList vybrane);
    void zmazPrikaz(Prikaz prikaz);
    void vlozPrikaz(Prikaz prikaz);

    static QByteArray hash(QStringList parametre);

    virtual QModelIndex index(int row, int column, const QModelIndex &parent) const Q_DECL_OVERRIDE;
    virtual QModelIndex parent(const QModelIndex &child) const Q_DECL_OVERRIDE;
    virtual int rowCount(const QModelIndex &parent) const Q_DECL_OVERRIDE;
    virtual int columnCount(const QModelIndex &parent) const Q_DECL_OVERRIDE;
    virtual QVariant data(const QModelIndex &index, int role) const Q_DECL_OVERRIDE;
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const Q_DECL_OVERRIDE;
    virtual bool removeRows(int row, int count, const QModelIndex &parent) Q_DECL_OVERRIDE;
};

#endif // HISTORIAMODEL_H
