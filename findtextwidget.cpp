/**
 ** This file is part of the Octopus project.
 ** Copyright 2023 wahibre <wahibre@gmx.com>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include <QHBoxLayout>
#include <QPushButton>
#include <QGraphicsOpacityEffect>
#include <QEvent>
#include <QApplication>
#include <QPalette>
#include <QTimer>

#include "findtextwidget.h"

/***********************************************************************/
FindTextWidget::FindTextWidget(QWidget *parent) : QWidget(parent)
{
    casesensitive=false;
    createUI(parent);
}
/***********************************************************************/
void FindTextWidget::createUI(QWidget *parent)
{
                  v  = new QVBoxLayout(parent);
                  e  = new QLineEdit();
            auto *t  = new QTimer(this);
            auto *h  = new QHBoxLayout(this);
            auto *vc = new QVBoxLayout();
            auto *bl = new QPushButton("<",  this);
            auto *br = new QPushButton(">",  this);
            auto *cs = new QPushButton("Aa", this);

    focusableWidgets << e << bl << br << cs;

    QGraphicsOpacityEffect *opac = new QGraphicsOpacityEffect(this);
    opac->setOpacity(0.90);

    setVisible(false);
    setGraphicsEffect(opac);

    QPalette p = e->palette();
    p.setColor(QPalette::Text, Qt::darkMagenta);
    e->setPalette(p);
    e->setPlaceholderText(L_ZADAJ_TEXT_HLADAJ);
    e->setClearButtonEnabled(true);

    bl->setMaximumWidth(18);
    bl->setToolTip(TT_SPAT);
    br->setMaximumWidth(18);
    br->setToolTip(TT_DALEJ);
    cs->setMaximumWidth(22);
    cs->setToolTip(TT_CASE_SENSITIVE);
    cs->setCheckable(true);

    v->addWidget(this);
    v->addStretch();

    h->addStretch(0);
    h->setSpacing(1);

#if (QT_VERSION <= QT_VERSION_CHECK(5, 13, 0))
    h->setMargin(0);
#endif
    h->addWidget(e);
    h->addWidget(bl);
    h->addWidget(br);
    h->addWidget(cs);
    h->addLayout(vc);
    h->addStretch(0);

    t->setInterval(400);
    t->setSingleShot(true);

    connect(bl, &QPushButton::clicked,      [=](bool){emit hladaj(e->text(), casesensitive, true);});
    connect(br, &QPushButton::clicked,      [=](bool){emit hladaj(e->text(), casesensitive, false);});
    connect(cs, &QPushButton::toggled,      this, &FindTextWidget::caseSensitiveToggled);
    connect(e,  &QLineEdit::returnPressed,  this, &FindTextWidget::stlacenyEnter);
    connect(e,  &QLineEdit::textEdited,     [=](){t->start();});
    connect(t,  &QTimer::timeout,           this, &FindTextWidget::emitujZmenu);
}
/***********************************************************************/
void FindTextWidget::emitujZmenu()
{
    emit textEditedDelayed(e->text(), casesensitive);
}
/***********************************************************************/
void FindTextWidget::hladacikZobraz()
{
    setVisible(true);
}
/***********************************************************************/
void FindTextWidget::hladacikSkri()
{
    setVisible(false);
}
/***********************************************************************/
void FindTextWidget::hladacikZobraz(QString selectedText)
{
    if(!selectedText.isEmpty())
        e->setText(selectedText);
    e->setFocus();
    hladacikZobraz();
}
/***********************************************************************/
bool FindTextWidget::hasFocus()
{
    foreach(auto i, focusableWidgets)
        if(i->hasFocus())
            return true;

    return false;
}
/***********************************************************************/
void FindTextWidget::setFocus()
{
    e->setFocus();
    e->selectAll();
}
/***********************************************************************/
void FindTextWidget::stlacenyEnter()
{
    bool backwards = (QApplication::keyboardModifiers() & Qt::ShiftModifier);

    emit hladaj(e->text(), casesensitive, backwards);
}
/***********************************************************************/
void FindTextWidget::caseSensitiveToggled(bool jeStlacene)
{
    casesensitive = jeStlacene;
    emitujZmenu();
}
/***********************************************************************/
