/**
 ** This file is part of the Octopus project.
 ** Copyright 2023 wahibre <wahibre@gmx.com>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#ifndef WINENVIRONMENT_H
#define WINENVIRONMENT_H

#include <QDialog>
#include "prikaz.h"
#define COLUMN_TEXT_SEPARATOR "\t"
#define ROW_TEXT_SEPARATOR "\n"

namespace Ui {
class winEnvironment;
}

class winEnvironment : public QDialog
{
    Q_OBJECT

public:
    winEnvironment(const ENV env, QWidget *parent=0);
    ~winEnvironment();
    ENV getEnvs();
private:
    Ui::winEnvironment *ui;
    ENV environmenty;
    void NastaveniaZapis();
    void NastaveniaCitaj();
    void addRow();
    void addRow(int riadok, QString env=QString(), QString hodnota=QString());
    void clearAllRows();
public slots:
    bool copyToClipboard();
    bool pasteFromClipboard();
public:
    virtual bool eventFilter(QObject *watched, QEvent *event) Q_DECL_OVERRIDE;
};

#endif // WINENVIRONMENT_H
