/**
 ** This file is part of the Octopus project.
 ** Copyright 2023 wahibre <wahibre@gmx.com>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#ifndef PRIKAZ_H
#define PRIKAZ_H

#include <QStringList>
#include <QHash>
#include <QDomElement>

typedef QHash<QString, QString> ENV;

class Prikaz
{
public:
    Prikaz();
    Prikaz(QString program, QStringList parametre, ENV environment, QString pracovnyPriecinok);
    QString dajProgram();
    QString dajPracovnyPriecinok();
    QStringList dajParametre();
    QStringList dajParametreEscaped();
    ENV dajEnvironmenty();
    void majPracovnyPriecinok(const QString priecinok);
    void majProgramParametre(const QString program, const QStringList parametre);
    void majEnvironmenty(const ENV environmenty);
    bool porovnaj(Prikaz p);
    QByteArray hash();

    static Prikaz fromDomElement(QDomElement element);
    QString toScript();
private:
    QString program;
    QStringList parametre;
    QString pracovnyPriecinok;
    ENV environmenty;
    QByteArray _hash;
    void hashuj();
};
#endif // PRIKAZ_H
