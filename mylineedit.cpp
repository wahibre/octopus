/**
 ** This file is part of the Octopus project.
 ** Copyright 2023 wahibre <wahibre@gmx.com>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include <QDrag>
#include <QMimeData>
#include <QApplication>
#include <QDate>
#include <QAction>
#include <QMenu>
#include <QDir>
#include "mylineedit.h"

/***********************************************************************/
MyLineEdit::MyLineEdit(QWidget *parent):QLineEdit(parent)
{
    setToolTip(tr("Vyznačenú časť textu (celé číslo alebo dátum) je možné inkrementovať/dekrementovať pomocou NUM+, NUM-, NUM* alebo kolieskom myši"));
}
/***********************************************************************/
void MyLineEdit::setIndexUmiestnenia(int idx)
{
    indexUmiestnenia = idx;
    setStyleSheet(styleSheet() + " MyLineEdit { "
                                 "border-radius: 8px; "
                                 "border: 1px solid green;"
                                 "padding-left: 5px;}");
}
/***********************************************************************/
bool MyLineEdit::isSelectionNumber(int &outNumber)
{
    int cislo;
    bool selection_is_number=false;
    cislo = selectedText().toInt(&selection_is_number, 10);

    if(selection_is_number)
        outNumber = cislo;
    return selection_is_number;
}
/***********************************************************************/
bool MyLineEdit::isSelectionDate(QDate &outDate)
{
    bool selection_is_date=false;
    QDate datum = QDate::fromString(selectedText(), "d.M.yyyy");

    selection_is_date = datum.isValid();

    if(selection_is_date)
        outDate = datum;

    return selection_is_date;
}
/***********************************************************************/
bool MyLineEdit::isLastDayOfMonth(QDate datum)
{
    return (datum.daysInMonth() == datum.day());
}
/***********************************************************************/
bool MyLineEdit::increaseSelectedText(int step, Qt::KeyboardModifiers mod)
{
    int cislo;
    QDate datum;
    QString retazec = text();
    const int selStart = selectionStart();
    const int selLength = selectedText().length();

    if(isSelectionNumber(cislo))
    {
        cislo += step;
        QString cislo_ako_text = QString::number(cislo, 10);
        retazec.replace(selStart, selLength, cislo_ako_text);
        setText(retazec);
        setSelection(selStart, cislo_ako_text.length());

        return true;
    }

    if(isSelectionDate(datum))
    {
        QString datum_ako_text =increaseDateText(datum, step, mod);
        retazec.replace(selStart, selLength, datum_ako_text);
        setText(retazec);
        setSelection(selStart, datum_ako_text.length());

        return true;
    }
    return false;
}
/***********************************************************************/
QString MyLineEdit::increaseDateText(QDate datum, int step, Qt::KeyboardModifiers mod)
{
    QDate novyDatum;

    if( mod&Qt::ShiftModifier && mod&Qt::ControlModifier )  //CTRL+SHIFT
    {
        bool endOfMonth = isLastDayOfMonth(datum);

        novyDatum = datum.addYears(step);

        if(endOfMonth && !isLastDayOfMonth(novyDatum))
            novyDatum.setDate(novyDatum.year(), novyDatum.month(), novyDatum.daysInMonth());
    }
    else
    if (mod&Qt::ShiftModifier || mod&Qt::ControlModifier )  //CTRL or SHIFT
    {
        bool endOfMonth = isLastDayOfMonth(datum);

        novyDatum = datum.addMonths(step);

        if(endOfMonth && !isLastDayOfMonth(novyDatum))
            novyDatum.setDate(novyDatum.year(), novyDatum.month(), novyDatum.daysInMonth());
    }
    else
    if( (mod&(Qt::ControlModifier|Qt::ShiftModifier)) == 0) //none of CTRL,SHIFT
        novyDatum = datum.addDays(step);
    else
        novyDatum = datum;

    return novyDatum.toString("dd.MM.yyyy");
}
/***********************************************************************/
void MyLineEdit::dropEvent(QDropEvent *e)
{
    e->accept();
    if(e->mimeData()->hasUrls())
    {
        if(e->mimeData()->urls().first().isLocalFile())
            setText(QDir::toNativeSeparators(e->mimeData()->urls().first().toLocalFile()));
        else
            setText(e->mimeData()->text());
    }
    else if(e->mimeData()->hasText())
        setText(e->mimeData()->text());
    else if(e->mimeData()->hasHtml())
        setText(e->mimeData()->html());
    else if(e->mimeData()->hasFormat(O_MIME_LINEEDIT))
    {
        emit ziadostNaVymenu(e->mimeData()->data(O_MIME_LINEEDIT).toInt(),  /*index zdroj. LineEdit*/
                             indexUmiestnenia);                             /*index ciel.  LineEdit*/
    }
    else
        e->ignore();
}
/***********************************************************************/
void MyLineEdit::dragEnterEvent(QDragEnterEvent *e)
{
    if(e->mimeData()->hasFormat(O_MIME_LINEEDIT))
    {
        e->setAccepted(e->mimeData()->data(O_MIME_LINEEDIT).toInt() != indexUmiestnenia);
    }
    else    //filename, text, ...
    {
        e->acceptProposedAction();
        QLineEdit::dragEnterEvent(e);
    }
}
/***********************************************************************/
void MyLineEdit::mousePressEvent(QMouseEvent *e)
{
    /* CTRL + MouseLeftButton */
    if(e->button() == Qt::LeftButton && (QApplication::keyboardModifiers() & Qt::ControlModifier))
    {
        QByteArray data;
        QMimeData *mime = new QMimeData;
        QDrag *d = new QDrag(this);

        data.setNum(indexUmiestnenia);
        mime->setData(O_MIME_LINEEDIT, data);
        d->setMimeData(mime);
        d->setPixmap(grab());

        d->exec();
    }
    else
        QLineEdit::mousePressEvent(e);
}
/***********************************************************************/
void MyLineEdit::keyPressEvent(QKeyEvent *event)
{
    bool spracovane=false;

    if( hasSelectedText() )
    {
        switch (event->key()) {
        case Qt::Key_Plus:
            spracovane = increaseSelectedText( 1, event->modifiers());
            break;
        case Qt::Key_Minus:
            spracovane = increaseSelectedText(-1, event->modifiers());
            break;
        case Qt::Key_Asterisk:
        {
            QDate dummy;
            if(isSelectionDate(dummy))
            {
                int selStart = selectionStart();
                QString retazec = text();
                QString novyDatumStr = QDate::currentDate().toString("dd.MM.yyyy");
                retazec.replace(selStart, selectedText().length(), novyDatumStr);
                setText(retazec);
                setSelection(selStart, novyDatumStr.length());
                spracovane = true;
            }
        }
            break;
        default:
            spracovane=false;
            break;
        }
    }

    if(spracovane)
        event->accept();
    else
        QLineEdit::keyPressEvent(event);
}
/***********************************************************************/
void MyLineEdit::wheelEvent(QWheelEvent *event)
{
    if(hasSelectedText())
    {
        const int step = event->angleDelta().y()/8/15;

        if(increaseSelectedText(step, event->modifiers()))
        {
            event->accept();
            return;
        }
    }
    QLineEdit::wheelEvent(event);
}
/***********************************************************************/
void MyLineEdit::contextMenuEvent(QContextMenuEvent *event)
{
    QLineEdit *paramEdit = this;
    QString dnesStr = QDate::currentDate().toString("dd.MM.yyyy");
    QMenu* menu = createStandardContextMenu();
    QString paramText = paramEdit->selectionStart()!=-1 ? paramEdit->selectedText() : paramEdit->text();
    QAction ActFilterThis(QString("%1 \"%2\"").arg(tr("&Filter v histórii na")).arg(paramText), this);
    QAction ActInsToday(QString("%1 %2").arg(tr("&Vložiť hodnotu")).arg(dnesStr), this);

    connect(&ActInsToday, &QAction::triggered, [paramEdit, dnesStr] () {
        QString novy, retazec = paramEdit->text();
        int cpos = paramEdit->cursorPosition();

        novy = retazec.insert(cpos, dnesStr);
        paramEdit->setText(novy);
        paramEdit->setCursorPosition(cpos+dnesStr.length());
    });

    connect(&ActFilterThis, &QAction::triggered, [=]() {
        emit ziadostNaFilter(paramText);
    });

    menu->addSeparator();
    if(!paramText.isEmpty())
        menu->addAction(&ActFilterThis);
    menu->addAction(&ActInsToday);
    menu->exec(mapToGlobal(event->pos()));
    menu->deleteLater();

    event->accept();
}
/***********************************************************************/
