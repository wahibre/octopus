/**
 ** This file is part of the Octopus project.
 ** Copyright 2023 wahibre <wahibre@gmx.com>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include <QClipboard>
#include <QMimeData>
#include <QSettings>
#include "ui_winenvironment.h"
#include "winenvironment.h"
#include "autotooltipdelegate.h"
#include "konstanty.h"

winEnvironment::winEnvironment(const ENV env, QWidget *parent):
    QDialog(parent),
    ui(new Ui::winEnvironment),
    environmenty(env)
{
    ui->setupUi(this);

    ui->tableWidget->installEventFilter(this);

    ENV::const_iterator iter;
    int riadok=0;

    for(iter = env.constBegin(); iter!=env.constEnd(); iter++, riadok++)
        addRow(riadok, iter.key(), iter.value());

    addRow(riadok);

    auto t = ui->tableWidget;
    t->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    t->horizontalHeader()->setStretchLastSection(true);
    t->setItemDelegate(new MyAutoToolTipDelegate(t));
    t->addAction(ui->actionCopy);
    t->addAction(ui->actionPaste);
    t->addAction(ui->actionZmazat);
    t->addAction(ui->actionZmazatVsetko);

    connect(ui->actionCopy, &QAction::triggered, this, &winEnvironment::copyToClipboard);
    connect(ui->actionPaste, &QAction::triggered, this, &winEnvironment::pasteFromClipboard);

    connect(ui->actionZmazat, &QAction::triggered, [=](){
        foreach (auto i, ui->tableWidget->selectedItems()) {
            i->setText(QString());
        }
    });

    connect(ui->actionZmazatVsetko, &QAction::triggered, [=](){
        ui->tableWidget->setRowCount(0);
        addRow();
    });

    connect(t, &QTableWidget::cellPressed, [=](int cellRow, int){
        if(cellRow == (t->rowCount()-1))
            addRow();
    });

    NastaveniaCitaj();
}

winEnvironment::~winEnvironment()
{
    NastaveniaZapis();
}

ENV winEnvironment::getEnvs()
{
    ENV envList;
    for(int i=0; i < ui->tableWidget->rowCount(); i++)
    {
        if(ui->tableWidget->item(i, 0) && !ui->tableWidget->item(i, 0)->text().isEmpty())
            envList[ui->tableWidget->item(i, 0)->text()] = ui->tableWidget->item(i, 1)->text();
    }
    return envList;
}

void winEnvironment::NastaveniaZapis()
{
    QScopedPointer<QSettings> nastavenia(new QSettings(this));

    nastavenia->beginGroup("winEnvironment");
    nastavenia->setValue("Size", size());
    nastavenia->setValue(REG_WIDTH_COL_0, ui->tableWidget->columnWidth(0));
    nastavenia->endGroup();
}

void winEnvironment::NastaveniaCitaj()
{
    QScopedPointer<QSettings>   nastavenia(new QSettings(this));
    nastavenia->beginGroup("winEnvironment");

    if(nastavenia->contains("Size"))
        resize(nastavenia->value("Size", QSize()).toSize());
    ui->tableWidget->setColumnWidth(0, nastavenia->value(REG_WIDTH_COL_0, 190).toInt());

    nastavenia->endGroup();
}

void winEnvironment::addRow()
{
    addRow(ui->tableWidget->rowCount(), "", "");
}

void winEnvironment::addRow(int riadok, QString env, QString hodnota)
{
    ui->tableWidget->insertRow(riadok);
    ui->tableWidget->setItem(riadok, 0, new QTableWidgetItem(env));
    ui->tableWidget->setItem(riadok, 1, new QTableWidgetItem(hodnota));
}

void winEnvironment::clearAllRows()
{
    ui->tableWidget->clearContents();
    ui->tableWidget->setRowCount(0);
}

bool winEnvironment::copyToClipboard()
{
    int nrSelected = ui->tableWidget->selectedItems().size();
    if(nrSelected>0 && nrSelected % 2 == 0)
    {
        QStringList sl;
        QClipboard *clip = QGuiApplication::clipboard();

        for(int i=0; i<nrSelected; i+=2)
            sl.append(  ui->tableWidget->selectedItems()[i]->text() +
                        COLUMN_TEXT_SEPARATOR +
                        ui->tableWidget->selectedItems()[i+1]->text());

        auto mimeData = new QMimeData();

        mimeData->setText(sl.join(ROW_TEXT_SEPARATOR));
        clip->setMimeData(mimeData);
        return true;
    }
    return false;
}

bool winEnvironment::pasteFromClipboard()
{
    QClipboard *clip = QGuiApplication::clipboard();
    auto mimeData = clip->mimeData();

    if(mimeData->hasText())
    {
        QStringList sl = mimeData->text().split(ROW_TEXT_SEPARATOR);
        if(sl.size() > 0)
        {
            clearAllRows();
            int rowid = 0;

            foreach(auto line, sl)
            {
                QStringList envPair;
                if(line.contains(COLUMN_TEXT_SEPARATOR))
                {
                    envPair = line.split(COLUMN_TEXT_SEPARATOR);

                    if(envPair.size() == 2)
                        addRow(rowid++, envPair[0], envPair[1]);
                }
            }

            return true;
        }
    }

    return false;
}

bool winEnvironment::eventFilter(QObject *watched, QEvent *event)
{
    if(watched == ui->tableWidget)
    {
        if(event->type() == QEvent::KeyPress)
        {
            QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);

            if(keyEvent->matches(QKeySequence::Copy))
                return copyToClipboard();

            if(keyEvent->matches(QKeySequence::Paste))
                return pasteFromClipboard();
        }
    }
    return QObject::eventFilter(watched, event);
}
