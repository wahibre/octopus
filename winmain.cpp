/**
 ** This file is part of the Octopus project.
 ** Copyright 2023 wahibre <wahibre@gmx.com>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include "winmain.h"
#include "ui_winmain.h"
#include "autotooltipdelegate.h"
#include "mylineedit.h"
#include "winenvironment.h"

#include <QInputDialog>
#include <QFileDialog>
#include <QClipboard>
#include <QXmlSchema>
#include <QXmlSchemaValidator>
#include <QDesktopServices>
#include <QFileSystemModel>
#include <QDialog>
#include <QLabel>
#include <QIcon>
#include <QStyle>

/******************************************************************************************/
winMain::winMain(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::winMain)
{
    ui->setupUi(this);    

    setWindowTitle(QString("%1 %2 (%3)").arg(qAppName()).arg(VERZIA).arg(BUILDDATE));

    finder = new FindTextWidget(ui->tableView);
    connect(finder, &FindTextWidget::hladaj, this, &winMain::hladajHistoria);
    connect(finder, &FindTextWidget::textEditedDelayed, this, &winMain::updateHistoriaViewPort);

    QStringList btnRunTooltip;
    btnRunTooltip.append(tr("Spustiť proces"));
    btnRunTooltip.append(tr("alebo"));
    btnRunTooltip.append(tr("Spustiť proces na pozadí pomocou SHIFT"));
#if (QT_VERSION >= QT_VERSION_CHECK(5, 10, 0))
    ui->actionSpustiProcesOdpojene->setVisible(true);
    btnRunTooltip.append(tr("alebo"));
    btnRunTooltip.append(tr("Spustiť proces samostatne pomocou CTRL"));
#endif
    ui->btnRun->setToolTip(btnRunTooltip.join("\n"));

    QFileSystemModel *filesystem = new QFileSystemModel(this);
    filesystem->setFilter(QDir::AllDirs | QDir::Drives | QDir::AllEntries | QDir::NoDotAndDotDot /*| QDir::Dirs | QDir::Files | QDir::Executable*/) ;
    filesystem->setRootPath("");

    /*nasepkavanie*/
    nasepkavacParam = new QCompleter(new QStringListModel(this), this);
    nasepkavacParam->setCaseSensitivity(Qt::CaseInsensitive);
    /*  nastavovane cez QSettings
    nasepkavacParam->setFilterMode(Qt::MatchContains);  */

    nasepkavacProg = new QCompleter(filesystem, this);
    nasepkavacProg->setCaseSensitivity(Qt::CaseInsensitive);
    nasepkavacProg->setModelSorting(QCompleter::CaseInsensitivelySortedModel);
    nasepkavacProg->setFilterMode(Qt::MatchStartsWith);
    ui->edtProgram->setCompleter(nasepkavacProg);
    connect(ui->edtProgram, &MyProgramLineEdit::ziadostNaFilter, this, &winMain::onFiltrujProgram);

    /*historia*/
    historiaFilterModel.setSourceModel(&historiaModel);
    ui->tableView->setModel(&historiaFilterModel);
    ui->tableView->addAction(ui->actionDelProgram);
    ui->tableView->addAction(ui->actionExportVybranych);
    ui->tableView->addAction(ui->actionLokalizuj_program);
    ui->tableView->addAction(ui->actionOtvor_prac_priecinok);
    ui->tableView->addAction(ui->actionHladaj);
    ui->tableView->horizontalHeader()->setToolTip(QString("%1<br><div style=\"color:grey\">%2</div>").arg("Kliknutím zadaj filter").arg("CTRL+Klik zruší filter"));
    ui->tableView->setWordWrap(false);
    ui->tableView->setItemDelegate(new MyAutoToolTipDelegate(ui->tableView));

    connect(ui->tableView->horizontalHeader(), SIGNAL(sectionClicked(int)), this, SLOT(historiaHlavickaClicked(int)));
    connect(QGuiApplication::clipboard(), SIGNAL(dataChanged()), this, SLOT(ClipBoardDataChanged()));

    NastavPracZlozku(qApp->applicationDirPath());
    ClipBoardDataChanged();
    setAcceptDrops(true);
    ui->btnEnvironment->hide();

    /* kontrola novej verzie */
    versionDownloader = new VersionDownloader;
    connect(versionDownloader, SIGNAL(zmenyNacitane(QString,bool)), this, SLOT(najdenaNovsiaVeria(QString,bool)));
	
	/* Windows Extra */
	#ifdef Q_OS_WIN
    thumbnailToolBar=Q_NULLPTR;
	#endif
}
/******************************************************************************************/
winMain::~winMain()
{
    NastaveniaZapis();   
    HistoriaZapis();

    if(versionDownloader && versionDownloader->getBeziVoVlastnomVlakne())
    {
        if(versionDownloader->thread()->isRunning())    //ak este bezi kontrola verzie,
            versionDownloader->thread()->wait(30000);   //pockam max. 30 sec.
        else
            delete versionDownloader;
    }

    delete ui;
}
/******************************************************************************************/
void winMain::NastaveniaZapis()
{
    QScopedPointer<QSettings> set(new QSettings(this));

    set->beginGroup("winMain");
        set->setValue("maximalizovane", isMaximized());
        if (!isMaximized())
        {
            set->setValue("pozicia", pos());
            set->setValue("velkost", size());
        }

    set->setValue(REG_WIDTH_COL_0, ui->tableView->columnWidth(0));

    if(historiaFilterModel.filter(0).isEmpty())
        set->remove(REG_FILTER_0);
    else
        set->setValue(REG_FILTER_0, historiaFilterModel.filter(0));

    if(historiaFilterModel.filter(1).isEmpty())
        set->remove(REG_FILTER_1);
    else
        set->setValue(REG_FILTER_1, historiaFilterModel.filter(1));

    set->setValue(REG_WORK_DIRECTORY, prikaz.dajPracovnyPriecinok());
    set->endGroup();
}
/******************************************************************************************/
void winMain::NastaveniaNacitaj()
{
    QScopedPointer<QSettings> set(new QSettings(this));

    set->beginGroup("winMain");

    if (!set->contains("pozicia"))
        return;

    move(set->value("pozicia", QPoint(0, 0)).toPoint());
    resize(set->value("velkost", QSize(400, 400)).toSize());
    if(set->value("maximalizovane", false).toBool())
        setWindowState(Qt::WindowMaximized);

    ui->tableView->setColumnWidth(0, set->value(REG_WIDTH_COL_0, 100).toInt());

    historiaFilterModel.nastavFilter(0, set->value(REG_FILTER_0, "").toString());
    historiaFilterModel.nastavFilter(1, set->value(REG_FILTER_1, "").toString());

    NastavPracZlozku(set->value(REG_WORK_DIRECTORY, "").toString());

    /*  Filter nasepkavaca (Qt::MatchFlag)
     *  POZOR! Je len readonly
        Qt::MatchContains	1	The search term is contained in the item.
        Qt::MatchStartsWith	2	The search term matches the start of the item.
        Qt::MatchEndsWith	3	The search term matches the end of the item.
    */
    nasepkavacParam->setFilterMode(static_cast<Qt::MatchFlag>(set->value(REG_COMPLETER_MODE, Qt::MatchStartsWith).toUInt()));
    set->endGroup();
}
/******************************************************************************************/
void winMain::HistoriaZapis()
{
    historiaModel.saveData();
}
/******************************************************************************************/
void winMain::HistoriaCitaj()
{
    if(historiaModel.loadData())
    {
        ui->tableView->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
        DoplnNasepkavacParametrov(historiaModel.vsetkyParametreHistorie());
    }
}
/******************************************************************************************/
void winMain::ClipBoardDataChanged()
{
    ui->action_VlozitParametre->setEnabled(QGuiApplication::clipboard()->mimeData(QClipboard::Clipboard)->hasText());
}
/******************************************************************************************/
void winMain::RemoveSelectedItemsFromHistory()
{
    historiaModel.zmazRiadky(
                historiaFilterModel.mapujNaModel(
                    ui->tableView->selectionModel()->selectedIndexes()
                    )
                );
}
/******************************************************************************************/
void winMain::AddItemToHistory(Prikaz prikaz)
{
    DoplnNasepkavacParametrov(prikaz.dajParametre());
    historiaModel.vlozPrikaz(prikaz);
    ui->tableView->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
}
/******************************************************************************************/
void winMain::RemParams(bool allWidgets=false)
{
    QLayoutItem *child;
    int wCount = ui->gridLayout->count();
    int jj;
    const int widgetGroup = 2;

    if (wCount >= widgetGroup)
    {
        jj= allWidgets?wCount:widgetGroup;

        for (int j=0; j<jj; j++)
        {
            child = ui->gridLayout->takeAt(ui->gridLayout->count()-1);      // vzdy posledne
            delete child->widget();
            delete child;
            ui->gridLayout->update();
        }
    }
}
/******************************************************************************************/
void winMain::DoplnNasepkavacParametrov(QStringList noveParametre)
{
    QStringListModel* model = qobject_cast<QStringListModel*>(nasepkavacParam->model());
    QStringList sl = model->stringList();

    sl.append(noveParametre);
    sl.removeDuplicates();
    model->setStringList(sl);
}
/******************************************************************************************/
void winMain::dragEnterEvent(QDragEnterEvent *event)
{
    if (event->mimeData()->hasFormat("text/uri-list"))
    {
        QList<QUrl> urls = event->mimeData()->urls();
        if (urls.isEmpty())
            return;

        QString fileName = urls.first().toLocalFile();
        if (fileName.isEmpty())
            return;

        QFileInfo fi(fileName);
        if(fi.exists(fileName) && fi.isExecutable())
        {
            event->acceptProposedAction();
        }
    }
}
/******************************************************************************************/
void winMain::showEvent(QShowEvent *)
{
#ifdef Q_OS_WIN
	processWatcher.setWindow(windowHandle());

    if(!thumbnailToolBar)
		vytvorThumbnailExecButton();
	else
		thumbnailToolBar->setWindow(windowHandle());
#endif
}
/******************************************************************************************/
void winMain::updateRunButtonProperty(QString mode)
{
    ui->btnRun->setProperty("RunProcessMode", mode);
    ui->btnRun->style()->unpolish(ui->btnRun);
    ui->btnRun->style()->polish(ui->btnRun);
    ui->btnRun->update();
}
/******************************************************************************************/
void winMain::keyPressEvent(QKeyEvent *event)
{
    if(event->modifiers() == Qt::ControlModifier)
        updateRunButtonProperty("dettached");
    else
        if(event->modifiers() == Qt::ShiftModifier)
            updateRunButtonProperty("quiet");

    if(event->key() == Qt::Key_Escape && finder->hasFocus())
    {
        finder->hladacikSkri();
        ui->tableView->setFocus();
        event->accept();
        return;
    }

    QMainWindow::keyPressEvent(event);
}
/******************************************************************************************/
void winMain::keyReleaseEvent(QKeyEvent *event)
{
    if((event->modifiers() & (Qt::ControlModifier|Qt::ShiftModifier)) == Qt::NoModifier)
        updateRunButtonProperty();

    QMainWindow::keyReleaseEvent(event);
}
/******************************************************************************************/
void winMain::dropEvent(QDropEvent *event)
{
    QString subor = event->mimeData()->urls().first().toLocalFile();
    QFileInfo fi(subor);

    if(fi.isExecutable())
        ui->edtProgram->setText(QDir::toNativeSeparators(subor));

}
/******************************************************************************************/
void winMain::AddParams()
{
    MyLineEdit *par;
    int rc = ui->gridLayout->count()/2;

    for (int i=0; i<2; i++)
    {
        par = new MyLineEdit(this);
        par->setCompleter(nasepkavacParam);
        par->setAcceptDrops(true);
        par->setClearButtonEnabled(true);

        ui->gridLayout->addWidget(par, rc, i);
        par->setIndexUmiestnenia(ui->gridLayout->indexOf(par));
        connect(par, &MyLineEdit::ziadostNaVymenu, this, &winMain::onVymenaLineEdit);
        connect(par, &MyLineEdit::ziadostNaFilter, this, &winMain::onFiltrujParametre);
    }
}
/******************************************************************************************/
void winMain::AddParamsFromList(QStringList parametre)
{
    int i=0;

    RemParams(true);

    foreach(QString item, parametre)
    {
        if(i%2 == 0)
            AddParams();

        qobject_cast<QLineEdit*>(ui->gridLayout->itemAt(i)->widget())->setText(item);
        qobject_cast<QLineEdit*>(ui->gridLayout->itemAt(i)->widget())->setCompleter(nasepkavacParam);

        i++;
    }
}
/******************************************************************************************/
QStringList winMain::CreateParamsList()
{
    QLineEdit* edit;
    QStringList listOfParameters;

    for(int i=0; i < ui->gridLayout->count(); i++)
    {
        edit = static_cast<QLineEdit*>(ui->gridLayout->itemAt(i)->widget());

        if(edit)
            listOfParameters.append(edit->text().simplified());
    }
    return listOfParameters;
}
/******************************************************************************************/
Prikaz& winMain::aktualizujPrikaz()
{
    prikaz.majProgramParametre(ui->edtProgram->text(), CreateParamsList());
    return prikaz;
}
/******************************************************************************************/
void winMain::ParseParamsFromClipboard()
{
    QClipboard *clip = QGuiApplication::clipboard();
    const QMimeData *mime = clip->mimeData(QClipboard::Clipboard);

    if(mime->hasText())
    {
        QString stringOfParameters;
        QStringList listOfParameters;
        bool quotedPart=false;
        stringOfParameters = mime->text();

        for(int i=0; i<stringOfParameters.size(); i++)
        {
            if(stringOfParameters[i] == QChar('"'))
                quotedPart = !quotedPart;

            if(stringOfParameters[i] == QChar(' ') && quotedPart)
                stringOfParameters[i] = QChar('\0');
        }

#if (QT_VERSION < QT_VERSION_CHECK(5, 15, 0))
        listOfParameters = stringOfParameters.split(" ", QString::KeepEmptyParts, Qt::CaseInsensitive);
#else
        listOfParameters = stringOfParameters.split(" ", Qt::KeepEmptyParts, Qt::CaseInsensitive);
#endif
        QMutableListIterator<QString> i(listOfParameters);

        while(i.hasNext())
        {
            i.next().replace('\0', ' ');
            i.value().replace(QString("\""), QString(""));
        }

        AddParamsFromList(listOfParameters);
    }
}
/******************************************************************************************/
void winMain::CopyParamsToClipboard()
{
    QClipboard *clip = QGuiApplication::clipboard();
    auto mimeData = new QMimeData();
    aktualizujPrikaz();
    QStringList list = prikaz.dajParametreEscaped();

    if(!list.isEmpty())
    {
        mimeData->setText(list.join(" "));
        clip->setMimeData(mimeData);
    }
}
/******************************************************************************************/
void winMain::RunProcessWin()
{
    RunProcess(beziaci_vpredu);
}
/******************************************************************************************/
void winMain::RunProcess(StartProcesu start)
{
    QFileInfo ProgrammInfo;
    QString ProgrammName;
    aktualizujPrikaz();
    StartProcesu rezimStartu = start;

    ProgrammName = prikaz.dajProgram();
    ProgrammInfo.setFile(ProgrammName);

    if(!ProgrammInfo.exists())
    {
        QString NajdenyProgram = QStandardPaths::findExecutable(ProgrammName);

        if (!NajdenyProgram.isEmpty())
            ProgrammInfo.setFile(NajdenyProgram);
        else
        {
            QMessageBox::critical(this, "Chyba", tr("Súbor %1 neexistuje!").arg(ProgrammName));
            return;
        }
    }
    if(!ProgrammInfo.isExecutable())
    {
        QMessageBox::critical(this, "Chyba", tr("Súbor %1 nie je spustiteľný!").arg(ProgrammName));
        return;
    }

    historiaModel.zmazPrikaz(prikaz);
    AddItemToHistory(prikaz);

    /* pre spustenie pomocou SHIFT/CTRL+mouse */
    if((QGuiApplication::keyboardModifiers()&Qt::ShiftModifier) != Qt::NoModifier)
        rezimStartu = beziaci_vzadu;
    if((QGuiApplication::keyboardModifiers()&Qt::ControlModifier) != Qt::NoModifier)
        rezimStartu = beziaci_mimo;

    winProcess *dlg = new winProcess(prikaz, rezimStartu, this);

    if(!prikaz.dajEnvironmenty().contains(IGNORE_TASKBAR_PROGRESS_ENV)
        && rezimStartu != beziaci_mimo)
    {
        connect(dlg, &winProcess::processStarted,  &processWatcher, &ProcessWatcher::show);
        connect(dlg, &winProcess::processFinished, &processWatcher, &ProcessWatcher::hide);
    }

    dlg->runProcess();
}
/******************************************************************************************/
void winMain::aktualizujEnvButtonStatus()
{
    ui->btnEnvironment->setVisible(prikaz.dajEnvironmenty().count() > 0);
}
/******************************************************************************************/
void winMain::onFiltrujProgram(QString vzor)
{
    filtruj(0, vzor);
}
/******************************************************************************************/
void winMain::onFiltrujParametre(QString vzor)
{
    filtruj(1, vzor);
}
/******************************************************************************************/
void winMain::NastavPracZlozku(QString zlozka)
{
    if(zlozka.isEmpty())
        prikaz.majPracovnyPriecinok(qApp->applicationDirPath());
    else
    {
        prikaz.majPracovnyPriecinok(zlozka);
        QDir::setCurrent(prikaz.dajPracovnyPriecinok());
    }
    ui->edtProgram->setToolTip(QString("<font color='#3300AA'><b>%1:</b></font>"
                                       "<p style='white-space:pre'><font color='#663300'>%2</font></p>"
                                       ).arg(tr("Pracovný priečinok")).arg(prikaz.dajPracovnyPriecinok().toHtmlEscaped()));
}
/******************************************************************************************/
void winMain::HistoriaViewExportSelected()
{
    QDomDocument dokExport;
    int pocetExportovanych;
    QString savefilename, NazovExportu;

    dokExport = historiaModel.vytvorDOM(
                    historiaFilterModel.mapujNaModel(ui->tableView->selectionModel()->selectedIndexes()),
                    pocetExportovanych
                );

    /* file dialog pre ulozenie xml */
    NazovExportu = QString("%1_%2.xml").arg(tr("Historia")).arg(QDateTime::currentDateTime().toString("yyyyMMdd_hh_mm_ss"));
    savefilename = QFileDialog::getSaveFileName(this, tr("Uložiť"), NazovExportu, "XML dokument (*.xml)");

    if (!savefilename.isEmpty())
    {
        QFile savefile(savefilename);
        if(savefile.open(QIODevice::WriteOnly|QIODevice::Text))
        {
            QTextStream ts(&savefile);
            ts.setCodec("UTF-8");
            dokExport.save(ts, 4, QDomNode::EncodingFromTextStream);
            savefile.close();
            QMessageBox::information(this, tr("Informácia"), QString("%1<br>%2: %3").arg(tr("Export ukončený")).arg(tr("Počet položiek")).arg(pocetExportovanych));
        }
        else
            QMessageBox::critical(this, tr("Chyba"), tr("Nie je možné otvoriť súbor %1 na zápis!").arg(savefilename));
    }
}
/******************************************************************************************/
void winMain::HistoriaImport()
{
    QString openfilename;

    openfilename = QFileDialog::getOpenFileName(this, tr("Otvoriť"), "", "XML dokument (*.xml)");

    if(!openfilename.isEmpty())
    {
        QFile openfile(openfilename),
                xsdfile(":historia.xsd");

        if(xsdfile.open(QIODevice::ReadOnly|QIODevice::Text))
        {
            QXmlSchema schema;
            schema.load(QUrl::fromLocalFile(xsdfile.fileName()));

            if(schema.isValid())
            {
                if(openfile.size()>0 && openfile.open(QIODevice::ReadOnly|QIODevice::Text))
                {
                    QXmlSchemaValidator validator(schema);

                    if (validator.validate(&openfile, QUrl::fromLocalFile(openfile.fileName())))
                    {
                        QDomDocument domHistImp;
                        QString errorStr;
                        int errorLine, errorColumn;
                        QString strparametre;
                        QStringList slPar;

                        /* QXmlSchemaValidator::validate - posuva poziciu v subore na koniec, musim ho vrati na zaciatok */
                        if(openfile.atEnd())
                            openfile.seek(0);

                        if (domHistImp.setContent(&openfile, true, &errorStr, &errorLine, &errorColumn))
                        {
                            QDomNodeList prikazy = domHistImp.elementsByTagName(HIST_ELEM_PRIKAZ);

                            for(int i=prikazy.count()-1; i>=0; i--)
                            {
                                QDomNodeList parametre = prikazy.at(i).toElement().elementsByTagName(HIST_ELEM_PARAMETER);
                                QDomNodeList environmentalky = prikazy.at(i).toElement().elementsByTagName(HIST_ELEM_ENVIRONMENT);

                                strparametre.clear();
                                slPar.clear();

                                for(int j=0; j<parametre.count(); j++)
                                {
                                    if (parametre.at(j).hasChildNodes())
                                        slPar << parametre.at(j).firstChild().nodeValue();

                                }

                                ENV e;
                                for(int k=0; k<environmentalky.count(); k++)
                                {
                                    e[environmentalky.at(k).attributes().namedItem(HIST_ATTR_NAZOV).nodeValue()]
                                            = environmentalky.at(k).firstChild().nodeValue();
                                }


                                AddItemToHistory(Prikaz(prikazy.at(i).attributes().namedItem(HIST_ELEM_PROGRAM).nodeValue(),
                                                        slPar,
                                                        e,
                                                        prikazy.at(i).attributes().namedItem(HIST_ELEM_WORKDIR).nodeValue()));
                            }

                            QMessageBox::information(this, tr("Informácia"), QString("%1<br>%2: %3").arg(tr("Import ukončený")).arg(tr("Počet položiek")).arg(prikazy.count()));
                        }
                        else
                            QMessageBox::critical(this, tr("Chyba"), QString("%1 %2, %3 %4: %5").arg(tr("Chyba spracovania XML na riadku")).arg(errorLine).arg(tr("stĺpec")).arg(errorColumn).arg(errorStr));
                    }
                    else
                        QMessageBox::critical(this, tr("Chyba"), tr("Súbor \"%1\" nemá požadovaný formát!").arg(openfile.fileName()));


                    openfile.close();
                }
                else
                    QMessageBox::critical(this, tr("Chyba"), tr("Nie je možné otvoriť súbor \"%1\" na čítanie!").arg(openfilename));

            }
            else
                QMessageBox::critical(this, tr("Chyba"), tr("XSD schéma nie je validná!"));

            xsdfile.close();
        }
        else
            QMessageBox::critical(this, tr("Chyba"), tr("Nie je možné otvoriť súbor \"%1\"").arg(xsdfile.fileName()));
    }
}
/******************************************************************************************/
void winMain::on_btnOpenProgram_clicked()
{
//    const QFileDialog::Options options = QFlag(fileDialogOptionsWidget->value());
    QString VychodziPriecinok, selectedFile, selectedFilter;

    const QString str_ShScript =    tr("Šel skript");
    const QString str_AllF =        tr("Všetko");

#ifdef Q_OS_WIN32
    const QString str_App =         tr("Aplikácia");
    QString fileFilter = QString("%1 (*.exe);;%2 (*.bat);;%3 (*.*)").arg(str_App).arg(str_ShScript).arg(str_AllF)
#else
    QString fileFilter = QString("%1 (*.*);;%2 (*.sh)").arg(str_AllF).arg(str_ShScript)
#endif
;

    auto prikaz = aktualizujPrikaz();
    QFileInfo fi(QDir(prikaz.dajPracovnyPriecinok()), prikaz.dajProgram());
    VychodziPriecinok = fi.absolutePath();

    if (!ui->edtProgram->text().isEmpty())
        VychodziPriecinok = ui->edtProgram->text();
    selectedFile = QFileDialog::getOpenFileName(
                this,                           //parent
                tr("Načítať binárku"),          //caption
                VychodziPriecinok,              //dir
                fileFilter,                     //filter
                &selectedFilter/*,              //selectedFilter
                options*/);                     //options

    if(!selectedFile.isEmpty())
        ui->edtProgram->setText(QDir::toNativeSeparators(selectedFile));
}
/*****************************************************************************************/
void winMain::on_tableView_doubleClicked(const QModelIndex &index)
{
    if(index.isValid())
    {
        ENV e;
        QStringList paramsList;
        Prikaz p = historiaFilterModel.dajPrikaz(index);
        prikaz = p;

        /* program */
        ui->edtProgram->setText(p.dajProgram());
        /* pracovna zlozka */
        NastavPracZlozku(p.dajPracovnyPriecinok());

        /* parametre */
        RemParams(true);
        paramsList = p.dajParametre();

        for(int i=0; i<paramsList.length(); i++)
        {
            if(i%2 == 0)
                AddParams();

            qobject_cast<QLineEdit*>(ui->gridLayout->itemAt(i)->widget())->setText(paramsList[i]);
        }
        aktualizujEnvButtonStatus();
    }
}
/******************************************************************************************/
void winMain::on_actionDelProgram_triggered()
{
    RemoveSelectedItemsFromHistory();
}
/******************************************************************************************/
void winMain::on_actionSpusti_proces_triggered()
{
    RunProcess(beziaci_vpredu);
}
/******************************************************************************************/
void winMain::on_actionSpustiProcesPotichucky_triggered()
{
    RunProcess(beziaci_vpredu);
}
/*****************************************************************************************/
void winMain::on_actionSpustiProcesOdpojene_triggered()
{
    RunProcess(beziaci_mimo);
}
/******************************************************************************************/
void winMain::on_btnRun_clicked()
{
    RunProcess(beziaci_vpredu);
}
/******************************************************************************************/
void winMain::on_actionAboutQt_triggered()
{
    QMessageBox::aboutQt(this);
}
/******************************************************************************************/
void winMain::on_action_Ukoncit_triggered()
{
    this->close();
}
/******************************************************************************************/
void winMain::on_actionPridat_parametre_triggered()
{
    AddParams();
}
/******************************************************************************************/
void winMain::on_action_Odstranitparametre_triggered()
{
    RemParams();
}
/*****************************************************************************************/
void winMain::on_actionKopirovatParametre_triggered()
{
    CopyParamsToClipboard();
}
/*****************************************************************************************/
void winMain::on_action_VlozitParametre_triggered()
{
    ParseParamsFromClipboard();
}
/******************************************************************************************/
void winMain::on_btnAddParameter_clicked()
{
    AddParams();

}
/******************************************************************************************/
void winMain::on_btnRemParameter_clicked()
{
    RemParams();
}
/*****************************************************************************************/
void winMain::on_actionZmenitPracZlozku_triggered()
{
    QString dir = QFileDialog::getExistingDirectory(this, L_ZMENA_PRAC_ZLOZKY,
                                                    prikaz.dajPracovnyPriecinok(),
                                                    QFileDialog::ShowDirsOnly|QFileDialog::HideNameFilterDetails);

    if(!dir.isEmpty())
        NastavPracZlozku(QDir::toNativeSeparators(dir));
}
/*****************************************************************************************/
void winMain::on_actionExportVybranych_triggered()
{
    HistoriaViewExportSelected();
}
/*****************************************************************************************/
void winMain::on_actionImportHistoria_triggered()
{
    HistoriaImport();
}
/*****************************************************************************************/
void winMain::on_actionLokalizuj_program_triggered()
{
    auto selIdx = ui->tableView->currentIndex();
    if(selIdx.isValid())
        OtvorPriecinok(selIdx, historiaFilterModel.dajPrikaz(selIdx).dajProgram());
}
/*****************************************************************************************/
void winMain::on_actionOtvor_prac_priecinok_triggered()
{
    auto selIdx = ui->tableView->currentIndex();
    if(selIdx.isValid())
        OtvorPriecinok(selIdx, historiaFilterModel.dajPrikaz(selIdx).dajPracovnyPriecinok());
}
/*****************************************************************************************/
void winMain::on_actionAbout_triggered()
{
    QDialog *helpDialog = new QDialog(this);
    QTextBrowser *helpBrow = new QTextBrowser(helpDialog);
    QHBoxLayout *lay = new QHBoxLayout(helpDialog);

    helpBrow->setSource(QUrl("qrc:/rsc/help.html"));
    lay->addWidget(helpBrow);

    helpDialog->setWindowTitle(QApplication::applicationDisplayName() + " - help");
    helpDialog->setWindowFlags(Qt::Dialog|Qt::WindowCloseButtonHint/*|Qt::FramelessWindowHint*/);
    helpDialog->resize(600,400);
    helpDialog->exec();
    helpDialog->deleteLater();
}
/*****************************************************************************************/
void winMain::on_actionHladaj_triggered()
{
    if(finder->isVisible())
    {
        finder->hladacikSkri();
        ui->tableView->setFocus();
    }
    else
    {
        finder->hladacikZobraz();
        finder->setFocus();
    }
}
/*****************************************************************************************/
void winMain::on_actionExportAsScript_triggered()
{
    Prikaz prikaz = aktualizujPrikaz();

    QFileInfo fi(QDir(prikaz.dajPracovnyPriecinok()), prikaz.dajProgram());

    QString subor = fi.baseName()+".bat";

    subor = QFileDialog::getSaveFileName(this, tr("Export ako skript"), subor, QString("*.%1").arg("bat"));

    if(!subor.isEmpty())
    {
        QFile f(subor);

        if(f.open(QIODevice::WriteOnly | QIODevice::Text))
        {
            QTextStream ts(&f);
            ts.setCodec("UTF-8");
            ts << prikaz.toScript();
            f.close();
        }
    }
}
/*****************************************************************************************/
void winMain::closeEvent(QCloseEvent *event)
{
    QString aktivneProcesy;

    foreach(winProcess *obj, findChildren<winProcess*>("winProcess", Qt::FindDirectChildrenOnly))
    {
        if(obj->jeProcesAktivny())
            aktivneProcesy += "<br>"+obj->nazovProgramu();
    }

    if(!aktivneProcesy.isEmpty())
            if(QMessageBox::warning(this,
                                    tr("Varovanie"),
                                    QString("<b>%1</b></b>%2").arg("Niektoré procesy sú stále aktívne. Naozaj chcete skončiť?").arg(aktivneProcesy),
                                    QMessageBox::Yes,
                                    QMessageBox::No
            ) == QMessageBox::No)
                event->ignore();
}
/*****************************************************************************************/
void winMain::filtruj(int idx, QString filterTxt)
{
    historiaFilterModel.nastavFilter(idx, filterTxt);
}
/*****************************************************************************************/
void winMain::historiaHlavickaClicked(int idx)
{
    QString filterTxt = historiaFilterModel.filter(idx);
    if(filterTxt.isEmpty() && ui->tableView->currentIndex().isValid())
    {
        filterTxt = historiaFilterModel.data(
                        historiaFilterModel.index(ui->tableView->currentIndex().row(), idx)
                    ).toString();
    }

    bool ok = true;

    if(QApplication::keyboardModifiers().testFlag(Qt::ControlModifier ))
        filterTxt = "";
    else
        filterTxt = QInputDialog::getText(this,
                                          tr("Zadaj filter"),
                                          QString("%1:").arg(historiaFilterModel.headerData(idx, Qt::Horizontal, Qt::DisplayRole).toString()),
                                          QLineEdit::Normal,
                                          filterTxt,
                                          &ok
#if (QT_VERSION < QT_VERSION_CHECK(5, 15, 0))
                                          , Q_NULLPTR, Q_NULLPTR
#endif
                                          );

    if(ok)
        filtruj(idx, filterTxt);
}
/******************************************************************************************/
void winMain::najdenaNovsiaVeria(const QString &text, const bool existujeNovsiaVerzia)
{
    if(existujeNovsiaVerzia)
    {
        ui->menubar->addAction(L_MENU_ZMENY_APP, this, SLOT(zobrazZmenyDlg(bool)));
        changelog = text;
    }
}
/******************************************************************************************/
void winMain::zobrazZmenyDlg(bool)
{
    QDialog *d = new QDialog(this);
    QVBoxLayout *l = new QVBoxLayout(d);
    QTextBrowser *t = new QTextBrowser(d);
    QLabel *dow = new QLabel(QString("<a href=\"%1\">%2</a>").arg(VersionDownloader::downloadURL()).arg(tr("Prevziať novú verziu")), d);


#if (QT_VERSION <= QT_VERSION_CHECK(5, 13, 0))
    l->setMargin(5);
#endif
    l->addWidget(t);
    l->addWidget(dow);
    t->setPlainText(changelog);
    d->setWindowTitle(L_WINT_ZMENY_APP);
    d->resize(450,300);
    dow->setOpenExternalLinks(true);

    d->exec();
}
/******************************************************************************************/
void winMain::onVymenaLineEdit(int indexZdroj, int indexCiel)
{
    MyLineEdit *zdroj, *ciel;
    QString txt;

    zdroj = static_cast<MyLineEdit*>(ui->gridLayout->itemAt(indexZdroj)->widget());
    ciel =  static_cast<MyLineEdit*>(ui->gridLayout->itemAt(indexCiel)->widget());

    if(!zdroj||!ciel)
        return;

    txt=ciel->text();
    ciel->setText(zdroj->text());
    zdroj->setText(txt);
}
/******************************************************************************************/
void winMain::hladajHistoria(QString text, bool caseSensitive, bool backward)
{
    auto curr = ui->tableView->currentIndex();
    auto nextIdx = QModelIndex();
    int step = backward ? -1 : 1;
    Qt::CaseSensitivity sensitivity = caseSensitive ? Qt::CaseSensitive : Qt::CaseInsensitive;

    if(historiaFilterModel.rowCount() > 0)
    {
        if(!curr.isValid())
            curr = historiaFilterModel.index(0,0);

        int r=curr.row();

        while ((nextIdx = historiaFilterModel.index(r += step, curr.column())).isValid())
        {
            if(historiaFilterModel.existujeTextVRiadku(nextIdx, text, sensitivity))
                break;
        }

        if(nextIdx.isValid())
            ui->tableView->selectionModel()->setCurrentIndex(nextIdx, QItemSelectionModel::Clear | QItemSelectionModel::SelectCurrent | QItemSelectionModel::Rows);

        updateHistoriaViewPort(text, caseSensitive);
    }
}
/******************************************************************************************/
void winMain::updateHistoriaViewPort(QString text, bool caseSensitive)
{
    historiaFilterModel.nastavFullTextSearch(text, caseSensitive ? Qt::CaseSensitive : Qt::CaseInsensitive);
    ui->tableView->viewport()->update();
}
/******************************************************************************************/
void winMain::OtvorPriecinok(QModelIndex selIdx, QString openDirFrom)
{
    QFileInfo fi(openDirFrom);

    if(fi.isFile() && !fi.exists())
        fi.setFile(QDir(historiaFilterModel.dajPrikaz(selIdx).dajPracovnyPriecinok()), openDirFrom);

    if(fi.exists())
    {
        if(fi.isDir())
            QDesktopServices::openUrl(QUrl::fromLocalFile(fi.absoluteFilePath()));
        else
        {
#ifdef Q_OS_WIN
            QStringList args;

            args << "/select,";
            args << QDir::toNativeSeparators(fi.absoluteFilePath());

            QProcess::startDetached("explorer", args);
#else
            QDesktopServices::openUrl(QUrl::fromLocalFile(fi.absoluteDir().absolutePath()));
#endif
        }
    }
}
/*****************************************************************************************/
void winMain::vytvorThumbnailExecButton()
{
#ifdef Q_OS_WIN
    thumbnailToolBar = new QWinThumbnailToolBar(this);
    thumbnailToolBar->setWindow(windowHandle());

    QWinThumbnailToolButton *thumbnailExecButton = new QWinThumbnailToolButton(thumbnailToolBar);
    thumbnailExecButton->setEnabled(true);
    thumbnailExecButton->setToolTip(tr("Spustiť proces"));
    thumbnailExecButton->setIcon(QIcon(":rsc/push.svg"));
    thumbnailExecButton->setDismissOnClick(true);

    thumbnailToolBar->addButton(thumbnailExecButton);

    connect(thumbnailExecButton, &QWinThumbnailToolButton::clicked, this, &winMain::RunProcessWin);
#endif
}
/*****************************************************************************************/
void winMain::on_actionEnvironment_triggered()
{
    winEnvironment d(prikaz.dajEnvironmenty(), this);
    d.exec();
    ENV envList = d.getEnvs();
    prikaz.majEnvironmenty(envList);
    aktualizujEnvButtonStatus();
}
/*****************************************************************************************/
