/**
 ** This file is part of the Octopus project.
 ** Copyright 2023 wahibre <wahibre@gmx.com>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include "zvyraznovac.h"
#include <QDebug>
#include <QApplication>
#include <QScrollBar>

/******************************************************************************************/
Zvyraznovac::Zvyraznovac(QTextEdit *textEditor, const QColor inFarbaPozadia, QObject *parent) :
    QObject(parent)
{
    editor = textEditor;
    farbaPozadia = inFarbaPozadia;
    budik.setInterval(ZVYRAZ_ONESKORENIE);
    connect(&budik, SIGNAL(timeout()), this, SLOT(zvyrazniText()));
}
/******************************************************************************************/
void Zvyraznovac::zvyrazniVybrane()
{
    budik.start();
}
/******************************************************************************************/
void Zvyraznovac::zvyrazniText()
{
    if(mutex.tryLock(0))
    {
        QString findStr = editor->textCursor().selectedText();

        if (!findStr.isEmpty())
        {
            const int poziciaScrollBaru = editor->verticalScrollBar()->value();
            kurzorVyberu = editor->textCursor();

            QList<QTextEdit::ExtraSelection> oznacene;

            editor->moveCursor(QTextCursor::Start, QTextCursor::MoveAnchor);

            while (editor->find(findStr))
            {
                QTextEdit::ExtraSelection es;
                es.format.setBackground(farbaPozadia);
                es.cursor = editor->textCursor();

                oznacene.append(es);
            }

            editor->setExtraSelections(oznacene);
            editor->setTextCursor(kurzorVyberu);
            editor->verticalScrollBar()->setValue(poziciaScrollBaru);
        }
        budik.stop();
        mutex.unlock();
    }
}
/******************************************************************************************/
