QT += core gui xml xmlpatterns network

win32:QT += winextras

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = octopus
TEMPLATE = app

VER_MAJ=1
VER_MIN=0
VER_PAT=0
VER_RC=
VERSION = $${VER_MAJ}.$${VER_MIN}.$${VER_PAT}

use_git_version {
    DEFINES += VERZIA=\\\"$$system(git describe --tags --abbrev=4)\\\" VER_MAJ=$${VER_MAJ} VER_MIN=$${VER_MIN} VER_PAT=$${VER_PAT}
}
else {
    VERZIASTR='\\"$${VERSION}$${VER_RC}\\"'
    DEFINES += VERZIA=\"$${VERZIASTR}\" VER_MAJ=$${VER_MAJ} VER_MIN=$${VER_MIN} VER_PAT=$${VER_PAT}
}
target.path = /usr/bin

ikona.path = /usr/share/pixmaps
ikona.files = podporne/octopus.png

desktop.path = /usr/share/applications
desktop.files = podporne/octopus.desktop

INSTALLS += target
unix: INSTALLS += ikona desktop

ICON = octopus.ico
RC_ICONS = rsc/octopus.ico

SOURCES +=  main.cpp                \
            winprocess.cpp          \
            logovanie.cpp           \
            winmain.cpp             \
            autotooltipdelegate.cpp \
            zvyraznovac.cpp         \
            mylineedit.cpp          \
            mytextbrowser.cpp       \
            findtextwidget.cpp      \
            versiondownloader.cpp   \
            processwatcher.cpp      \
            prikaz.cpp              \
            winenvironment.cpp      \
            myprogramlineedit.cpp   \
            historiamodel.cpp       \
            historiaproxymodel.cpp

HEADERS  += konstanty.h             \
            winprocess.h            \
            logovanie.h             \
            winmain.h               \
            autotooltipdelegate.h   \
            zvyraznovac.h           \
            mylineedit.h            \
            konstanty.h             \
            findtextwidget.h        \
            mytextbrowser.h         \
            versiondownloader.h     \
            processwatcher.h        \
            prikaz.h                \
            winenvironment.h        \
            myprogramlineedit.h     \
            historiamodel.h         \
            historiaproxymodel.h

FORMS   +=  winprocess.ui \
            winmain.ui \
            winenvironment.ui

OTHER_FILES +=  changelog.txt\
            podporne/help.md\
            podporne/make_help.sh\
            podporne/Octopus_Create_Setup.iss\
            buildrpm/octopus.spec\
            rsc/out_template.html\
            README.md\
            .gitlab-ci.yml

RESOURCES += resources.qrc

TRANSLATIONS = languages/en.ts

win32 {
    DEFINES += BUILDDATE=\\\"$$system('powershell get-date -format "{dd.MM.yyyy}"')\\\"
} else {
    DEFINES += BUILDDATE=\\\"$$system(date '+%d.%m.%Y')\\\"
}

# DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x050F00
