/**
 ** This file is part of the Octopus project.
 ** Copyright 2023 wahibre <wahibre@gmx.com>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#ifndef HISTORIAPROXYMODEL_H
#define HISTORIAPROXYMODEL_H

#include <QSortFilterProxyModel>
#include <QModelIndexList>

#include "prikaz.h"

class HistoriaProxyModel : public QSortFilterProxyModel
{
private:
   QMap<int, QString> HistHeadersFilter;

public:
    HistoriaProxyModel(QObject *parent = nullptr);
    void nastavFilter(int section, QString filter);
    void nastavFullTextSearch(QString text, Qt::CaseSensitivity sensitivity) const;
    QString filter(int section);
    Prikaz dajPrikaz(QModelIndex idx);
    QModelIndexList mapujNaModel(QModelIndexList list);
    bool existujeTextVRiadku(const QModelIndex &idx, QString text, Qt::CaseSensitivity caseSensitivity);
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const Q_DECL_OVERRIDE;

protected:
    bool virtual filterAcceptsRow(int source_row, const QModelIndex &source_parent) const Q_DECL_OVERRIDE;
};
#endif // HISTORIAPROXYMODEL_H
