GUI Application helps to execute multi-switched commands using history.
This Application uses [QT library](https://www.qt.io/).

Dependency
===========
**Fedora:**
    dnf install qt5-qtbase-devel qt5-qtxmlpatterns-devel qt5-qtsvg

Build
=====
    qmake-qt5 Octopus.pro
    make

Install
=======
    make install



### To generate HTML help (Slovak only):

```
$ podporne/make_help.sh
```

## Rererences
* [Octopus homepage](https://gitlab.com/wahibre/octopus/wikis/home)
* [Qt versions support](https://en.wikipedia.org/wiki/Qt_version_history)
