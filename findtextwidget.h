/**
 ** This file is part of the Octopus project.
 ** Copyright 2023 wahibre <wahibre@gmx.com>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#ifndef FINDWIDGET_H
#define FINDWIDGET_H

#include <QWidget>
#include <QLineEdit>
#include <QVBoxLayout>
#include <QFrame>
#include <QTextEdit>

//TODO zamenit closebutton za spendlik

class FindTextWidget : public QWidget
{
    Q_OBJECT
    const QString L_ZADAJ_TEXT_HLADAJ   = tr("Zadaj text na vyhľadávanie");
    const QString TT_CASE_SENSITIVE     = tr("Rozlišovať veľké písmená");
    const QString TT_SPAT               = tr("Späť");
    const QString TT_DALEJ              = tr("Ďalej");

private:
    QVBoxLayout *v;
    QLineEdit   *e;
    bool casesensitive;
    QList<QWidget*> focusableWidgets;

    void createUI(QWidget *parent);
public:
    explicit FindTextWidget(QWidget *parent);
    void hladacikZobraz();
    void hladacikZobraz(QString selectedText);
    bool hasFocus();
    void setFocus();
signals:
    void hladaj(QString text, bool caseSensitive, bool backward);
    void textEditedDelayed(QString text, bool caseSensitive);
private slots:
    void emitujZmenu();
public slots:
    void caseSensitiveToggled(bool jeStlacene);
    void hladacikSkri();
    void stlacenyEnter();
};

#endif // FINDWIDGET_H
