/**
 ** This file is part of the Octopus project.
 ** Copyright 2023 wahibre <wahibre@gmx.com>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include "versiondownloader.h"
#include "konstanty.h"
#include <QDomDocument>
#include <QThread>
#include <QTimer>

#include <QDebug>
#include <QNetworkProxy>
#include <QProcess>

VersionDownloader::VersionDownloader() : QObject(), beziVoVlastnomVlakne(false)
{
#ifndef QT_DEBUG
    verzieUrl = QUrl(QProcessEnvironment::systemEnvironment().value(VERSION_CHECK_URL_ENV, VERSION_CHECK_URL));
    if(verzieUrl.isValid())
    {
        QThread *versionCheckThread = new QThread();
        this->moveToThread(versionCheckThread);
        beziVoVlastnomVlakne = true;

        QNetworkProxy p;
        p.setType(QNetworkProxy::DefaultProxy);
        QNetworkProxy::setApplicationProxy(p);

        QTimer::singleShot(ONESKORENIE_KONTROLY_VERZIE_MS, versionCheckThread, SLOT(start()));
        connect(versionCheckThread, &QThread::started,  this, &VersionDownloader::zistiDostupneZmeny);
        connect(versionCheckThread, &QThread::finished, this, &VersionDownloader::deleteLater);
    }
#endif
}

void VersionDownloader::zistiDostupneZmeny()
{
    QNetworkReply* odpoved;
    QNetworkRequest poziadavka(verzieUrl);
    manager = new QNetworkAccessManager(this);

    odpoved = manager->get(poziadavka);
    odpoved->ignoreSslErrors();

    connect(manager, SIGNAL(finished(QNetworkReply*)), this, SLOT(spracujOdpoved(QNetworkReply*)));
    connect(odpoved, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(spracujChybu(QNetworkReply::NetworkError)));
    connect(odpoved, SIGNAL(sslErrors(QList<QSslError>)), this, SLOT(spracujSSLchyby(QList<QSslError>)));
}


bool VersionDownloader::getBeziVoVlastnomVlakne() const
{
    return beziVoVlastnomVlakne;
}

void VersionDownloader::spracujOdpoved(QNetworkReply * reply)
{
    QTextStream stream(reply);
    bool existujeNovsiaVerzia=false;

    if(reply->error() == QNetworkReply::NoError)
    {
        QDomDocument verzieDom;
        QString errorMsg;
        int errorLine, errorColumn;

        changelogText=" ";

        if(verzieDom.setContent(stream.readAll(), &errorMsg, &errorLine, &errorColumn))
        {
            QDomNodeList verzie = verzieDom.documentElement().childNodes();
            QDomElement e;
            QString vMaj,vMin,vPat,vDat;

            for(int i=0; i<verzie.count(); i++)
            {
                e = verzie.item(i).toElement();

                vMaj = e.attribute("maj");
                vMin = e.attribute("min");
                vPat = e.attribute("pat");
                vDat = e.attribute("datum");

                if( (VER_MAJ *1e6 + VER_MIN *1e3 + VER_PAT) < (vMaj.toInt()*1e6 + vMin.toInt()*1e3 + vPat.toInt()) )
                {
                    existujeNovsiaVerzia=true;
                    changelogText += QString("%1.%2.%3  (%4):").arg(vMaj).arg(vMin).arg(vPat).arg(vDat);
                    changelogText += verzie.item(i).toElement().text();
                    changelogText += "\n";
                }
            }

            emit zmenyNacitane(changelogText, existujeNovsiaVerzia);

        }
        else
            qWarning() << tr("changelog XML je v nesprávnom formáte")+QString(": %1 [ %2, %3 ]").arg(errorMsg).arg(errorLine).arg(errorColumn);

    }
    else
        qWarning() << reply->errorString();

    reply->deleteLater();
    QThread::currentThread()->quit();
}

void VersionDownloader::spracujChybu(QNetworkReply::NetworkError code)
{
    qWarning() << tr("Chyba spojenia:") << code;
}

void VersionDownloader::spracujSSLchyby(const QList<QSslError> &errors)
{
    foreach (QSslError ch, errors) {
        qWarning() << tr("Chyba SSL spojenia:") << ch.errorString();
    }
}
