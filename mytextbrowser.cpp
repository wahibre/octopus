/**
 ** This file is part of the Octopus project.
 ** Copyright 2023 wahibre <wahibre@gmx.com>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include "mytextbrowser.h"
#include <QPushButton>
#include <QKeyEvent>
#include <QDebug>

MyTextBrowser::MyTextBrowser(QWidget *parent): QTextBrowser(parent)
{
    finder = new FindTextWidget(this);
    akciaHladaj = new QAction(L_HLADAJ, parent);
    akciaHladaj->setShortcut(Qt::CTRL + Qt::Key_F);

    akciaZalamovanie = new QAction(L_ZALAMOVANIE, parent);
    akciaZalamovanie->setShortcut(Qt::CTRL + Qt::Key_W);
    akciaZalamovanie->setCheckable(true);

    connect(akciaHladaj, &QAction::triggered, this, &MyTextBrowser::hladacikZobrazit);
    connect(akciaZalamovanie, &QAction::toggled, this, &MyTextBrowser::toggleLineWrap);
    connect(finder, &FindTextWidget::hladaj, this, &MyTextBrowser::vyhladaj);
}

void MyTextBrowser::hladacikZobrazit(bool)
{
    finder->hladacikZobraz(textCursor().selectedText());
}

void MyTextBrowser::toggleLineWrap(bool checked)
{
    if(checked)
        setLineWrapMode(QTextEdit::WidgetWidth);
    else
        setLineWrapMode(QTextEdit::NoWrap);
}

void MyTextBrowser::vyhladaj(QString text, bool caseSensitive, bool backward)
{
    QTextDocument::FindFlags options;

    if(caseSensitive)
        options |= QTextDocument::FindCaseSensitively;

    if(backward)
        options |= QTextDocument::FindBackward;

    find(text, options);
}

void MyTextBrowser::keyPressEvent(QKeyEvent *ev)
{
    // Akcia na hladanie nie je viditelna, preto nefunguje skratka

    // CTRL+F
    if(ev->key() == Qt::Key_F && ev->modifiers() & Qt::ControlModifier)
        hladacikZobrazit(false);
    else
    {
        if(ev->key() == Qt::Key_W && ev->modifiers() & Qt::ControlModifier)
            akciaZalamovanie->toggle();
        else
            if(ev->key() ==Qt::Key_Escape && finder->hasFocus())
                finder->hladacikSkri();
            else
            {
                ev->ignore();
                QTextBrowser::keyPressEvent(ev);
                return;
            }
    }
    ev->accept();
}

bool MyTextBrowser::event(QEvent *e)
{
    if(e->type() == QEvent::FocusIn)
        finder->hladacikSkri();

    return QTextBrowser::event(e);
}

QMenu *MyTextBrowser::createStandardContextMenu(const QPoint &pos)
{
    QMenu *menicko = QTextBrowser::createStandardContextMenu(pos);
    menicko->addAction(akciaHladaj);
    menicko->addAction(akciaZalamovanie);
    return menicko;
}
