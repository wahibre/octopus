/**
 ** This file is part of the Octopus project.
 ** Copyright 2023 wahibre <wahibre@gmx.com>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include "processwatcher.h"

#ifdef Q_OS_WIN
#include <QRadialGradient>

/**********************
    Windows part
**********************/

ProcessWatcher::ProcessWatcher():
    activeProcesses(0)
{
    if(QSysInfo::windowsVersion() <= QSysInfo::WV_WINDOWS7)
    {
        progress()->setValue(50);
        progress()->setRange(0, 100);
    }
    else
        progress()->setRange(0, 0);
}

void ProcessWatcher::refreshOverlayIcon()
{
    if(activeProcesses > 1)
    {
        QPixmap px(iconSize, iconSize);
        px.fill(Qt::transparent);

        QFont f;
        f.setPixelSize(.75*iconSize);

        QRadialGradient rad(10, 10, .5*iconSize);
        rad.setCenter(px.rect().center());
        rad.setColorAt(0, Qt::white);
        rad.setColorAt(.7, Qt::green);
        rad.setColorAt(1, Qt::transparent);
        QBrush b(rad);

        QPainter p(&px);
        p.setBrush(b);
        p.setPen(Qt::NoPen);
        p.drawEllipse(0, 0, iconSize, iconSize);
        p.setFont(f);
        p.setPen(Qt::blue);
        p.drawText(0,0,iconSize,iconSize, Qt::AlignCenter, QString::number(activeProcesses));

        setOverlayIcon(QIcon(px));
    }
    else
        clearOverlayIcon();
}

void ProcessWatcher::show()
{
    if(activeProcesses++ <= 0)
        progress()->setVisible(true);

    refreshOverlayIcon();
}

void ProcessWatcher::hide()
{
    if(--activeProcesses <= 0)
        progress()->setVisible(false);

    refreshOverlayIcon();
}


#else
/**********************
        LINUX part
***********************/
ProcessWatcher::ProcessWatcher()
{

}

void ProcessWatcher::setWindow(QWindow*)
{

}
void ProcessWatcher::hide()
{

}
void ProcessWatcher::show()
{

}

#endif
