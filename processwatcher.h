/**
 ** This file is part of the Octopus project.
 ** Copyright 2023 wahibre <wahibre@gmx.com>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#ifndef PROCESSWATCHER_H
#define PROCESSWATCHER_H

#include <QObject>
#include <QWindow>

#ifdef Q_OS_WIN
#include <QtWinExtras>
#else
#endif

class ProcessWatcher: public
#ifdef Q_OS_WIN
        QWinTaskbarButton
#else
        QObject
#endif
{
    Q_OBJECT

    int activeProcesses;
    void refreshOverlayIcon();
    const int iconSize=32;

public:
    ProcessWatcher();

public slots:
    void show();
    void hide();

#ifndef Q_OS_WIN
    void setWindow(QWindow*);
#endif
};

#endif // PROCESSWATCHER_H
