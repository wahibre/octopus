/**
 ** This file is part of the Octopus project.
 ** Copyright 2023 wahibre <wahibre@gmx.com>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include "autotooltipdelegate.h"

MyAutoToolTipDelegate::MyAutoToolTipDelegate(QObject*parent): QStyledItemDelegate(parent)
{

}

bool MyAutoToolTipDelegate::helpEvent( QHelpEvent* e, QAbstractItemView* view, const QStyleOptionViewItem& option,
                                     const QModelIndex& index)
{
    if ( !e || !view )
        return false;

    if ( e->type() == QEvent::ToolTip )
    {
        QRect rect = view->visualRect( index );
        QSize size = sizeHint( option, index );
        if ( rect.width() < size.width() )
        {
            QVariant tooltip = index.data( Qt::DisplayRole );
            if ( tooltip.canConvert<QString>() )
            {
                const QString formatovanie="<p style='white-space:pre'><strong><span style=\"color: #3300AA;\">%1</span></strong></p>";

                /*HTML  tooltip */
                QToolTip::showText( e->globalPos(), formatovanie.arg(tooltip.toString().toHtmlEscaped()), view );
                return true;
            }
        }
        if ( !QStyledItemDelegate::helpEvent( e, view, option, index ) )
            QToolTip::hideText();
        return true;
    }

    return QStyledItemDelegate::helpEvent( e, view, option, index );
}
