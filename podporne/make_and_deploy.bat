@echo off

:: C:\Windows\System32\cmd.exe /A /Q /K C:\Qt\5.3\mingw482_32\bin\qtenv2.bat

::******************************************************
SET MINGW="C:\Qt\Tools\mingw482_32\bin\mingw32-make.exe"
SET QMAKE="C:\Qt\5.3\mingw482_32\bin\qmake.exe"
SET BUILDDIR=C:\Users\vm\Documents\building\Octopus
SET QDEPLOY=C:\Qt\5.3\mingw482_32\bin\windeployqt.exe
SET DEPLOYDIR="C:\Users\vm\Dropbox\_ACE\Octopus-neophyte-win"
SET SOURCEDIR=C:\Users\vm\Documents\svn\wahibresstuff.BinRunner\branches\devel
::******************************************************

echo ***************************************************************************
echo MINGW:     %MINGW:~1,-1%
echo QMAKE:     %QMAKE:~1,-1%
echo BUILDDIR:  %BUILDDIR%
echo QDEPLOY:   %QDEPLOY%
echo DEPLOYDIR: %DEPLOYDIR:~1,-1%
echo SOURCEDIR: %SOURCEDIR%
echo ***************************************************************************
echo.
pause

call  C:\Qt\5.3\mingw482_32\bin\qtenv2.bat

IF EXIST %BUILDDIR% DEL /Q %BUILDDIR%
mkdir %BUILDDIR%
pushd %BUILDDIR%

set PRIKAZ=%QMAKE% %SOURCEDIR%\BinRunner.pro -r -spec win32-g++
echo     ==^> %PRIKAZ%
%PRIKAZ%
if errorlevel 1 goto :chyba

set PRIKAZ=%MINGW%
echo     ==^> %PRIKAZ%
%PRIKAZ%
if errorlevel 1 goto :chyba


set PRIKAZ=%QDEPLOY% --libdir %DEPLOYDIR% %BUILDDIR%\release\Octopus.exe
echo     ==^> %PRIKAZ%
%PRIKAZ%
if errorlevel 1 goto :chyba

set PRIKAZ=xcopy /Y %BUILDDIR%\release\Octopus.exe %DEPLOYDIR%
echo     ==^> %PRIKAZ%
%PRIKAZ%
if errorlevel 1 goto :chyba

set PRIKAZ=xcopy /Y %SOURCEDIR%\changelog.txt %DEPLOYDIR%
echo     ==^> %PRIKAZ%
%PRIKAZ%
if errorlevel 1 goto :chyba

popd
echo.
ECHO KONIEC
ECHO.
pause
goto :eof


:spusti
    echo ^$%1
    %1
    if errorlevel 1 goto :chyba

    goto :eof

:chyba
    echo.
    echo *************
    echo !!! CHYBA !!!
    echo *************
    echo.
    pause
::    exit /b 1
