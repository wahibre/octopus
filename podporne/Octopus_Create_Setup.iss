#define MyAppName "Octopus"
#define MyAppVersion "1.0"
#define MyCompany "Rusty pipe"
#define MyAppExeName "octopus.exe"

;TODO pridat obrazok do setup.exe
[Setup]
SourceDir=Release
AppName={#MyAppName}
AppID={{CE18F61E-FA6D-4DEC-B1E7-FF60DB55C804}
AppVersion={#MyAppVersion}
AppVerName={#MyAppName} {#MyAppVersion}
AppPublisher={#MyCompany}
AppPublisherURL=https://gitlab.com/wahibre/octopus/wikis
DefaultDirName={pf}\{#MyCompany}\{#MyAppName}
UninstallDisplayIcon={app}\{#MyAppExeName}
SetupIconFile=octopus.ico
OutputDir=setup
OutputBaseFilename={#MyAppName}_setup_{#MyAppVersion}
DefaultGroupName={#MyAppName}
VersionInfoVersion={#MyAppVersion}
VersionInfoCompany={#MyCompany}
VersionInfoProductVersion={#MyAppVersion}
;VersionInfoDescription=description
;VersionInfoTextVersion=text_version
;VersionInfoCopyright=Copyright
VersionInfoProductName={#MyAppName}
[Files]
Source: D3Dcompiler_47.dll; DestDir: {app}
Source: libEGL.dll; DestDir: {app}
Source: libgcc_s_dw2-1.dll; DestDir: {app}
Source: libGLESV2.dll; DestDir: {app}
Source: libstdc++-6.dll; DestDir: {app}
Source: libwinpthread-1.dll; DestDir: {app}
Source: opengl32sw.dll; DestDir: {app}
Source: ssleay32.dll; DestDir: {app}
Source: libeay32.dll; DestDir: {app}
Source: octopus.exe; DestDir: {app}
Source: Qt5Core.dll; DestDir: {app}
Source: Qt5Gui.dll; DestDir: {app}
Source: Qt5Network.dll; DestDir: {app}
Source: Qt5Svg.dll; DestDir: {app}
Source: Qt5Widgets.dll; DestDir: {app}
Source: Qt5WinExtras.dll; DestDir: {app}
Source: Qt5Xml.dll; DestDir: {app}
Source: Qt5XmlPatterns.dll; DestDir: {app}
Source: platforms\qwindows.dll; DestDir: {app}\platforms\
Source: imageformats\qico.dll; DestDir: {app}\imageformats\
Source: imageformats\qjpeg.dll; DestDir: {app}\imageformats\
Source: imageformats\qsvg.dll; DestDir: {app}\imageformats\
Source: translations\qt_ca.qm; DestDir: {app}\translations\
Source: translations\qt_cs.qm; DestDir: {app}\translations\
Source: translations\qt_de.qm; DestDir: {app}\translations\
Source: translations\qt_en.qm; DestDir: {app}\translations\
Source: translations\qt_fi.qm; DestDir: {app}\translations\
Source: translations\qt_fr.qm; DestDir: {app}\translations\
Source: translations\qt_he.qm; DestDir: {app}\translations\
Source: translations\qt_hu.qm; DestDir: {app}\translations\
Source: translations\qt_it.qm; DestDir: {app}\translations\
Source: translations\qt_ja.qm; DestDir: {app}\translations\
Source: translations\qt_ko.qm; DestDir: {app}\translations\
Source: translations\qt_lv.qm; DestDir: {app}\translations\
Source: translations\qt_pl.qm; DestDir: {app}\translations\
Source: translations\qt_ru.qm; DestDir: {app}\translations\
Source: translations\qt_sk.qm; DestDir: {app}\translations\
Source: translations\qt_uk.qm; DestDir: {app}\translations\
;since Qt5.10
Source: styles\qwindowsvistastyle.dll; DestDir: {app}\styles\


[Dirs]
Name: {app}\platforms
Name: {app}\imageformats
Name: {app}\translations
Name: {app}\styles

[Run]
Filename: {app}\{#MyAppExeName}; Description: {cm:LaunchProgram,{#MyAppName}}; Flags: nowait postinstall skipifsilent

[Icons]
Name: {group}\Octopus; Filename: {app}\{#MyAppExeName}; WorkingDir: {app}; IconFilename: {app}\{#MyAppExeName}; IconIndex: 0
