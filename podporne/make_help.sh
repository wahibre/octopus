#!/bin/bash
set -e

# mkd2html is part of discount package

mkd2html -header "<style type="text/css"> h1,h2 {color: #4e9a06;  margin-bottom: 0} h1{text-align: center; background-color: #E0E0FF}</style>" podporne/help.md
mv podporne/help.html rsc/
