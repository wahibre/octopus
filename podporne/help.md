# Octopus

<table><tr>
    <td><img alt="{Octopus}" src=":octopus" width=128/></td>
    <td>Aplikácia je určená pre  spúšťanie súčasne viacerých procesov a sledovanie ich výstupu.</td>
</tr></table>

## 1. Výber programu
Prvým krokom je výber programu (spustiteľný súbor) jedným zo štyroch možností:

+ Výber cez štandardný dialóg
+ Vpísaním do textboxu pomocou našepkávania
+ Potiahnutím súboru do okna (Drag&Drop)
+ Vyvolaním z histórie

## 2. Parametre programu
* Parametre programu sú nepovinné. V prípade potreby zadať parametre je nutné ich najprv pridať. Pridávajú sa po dvoch v dvoch stĺpcoch (prepínač a hodnota). Nezadané parametre sú ignorované. Pri písaní hodnôt parametrov je aktivovaný **našepkávač**, ktorý obsahuje zozname všetkých použitých parametrov v histórii.
* Ak je **vyznačená časť textu celočíselná**, je možné číslo vzäčšiť (NUM+, mousescroll up) alebo zmenšiť (NUM-, mousescroll down).
* Ak je **vyznačená časť textu dátum**, je možné ho zväčšovať/zmenšovať obdobne ako číslo. Použitím kláves SHIFT a CTRL je možné inkrementovať dátum **po mesiacoch** (SHIFT alebo CTRL) alebo **rokoch** (SHIFT+CTRL). Klavesa NUM* nahradí vyznačený dátum aktuálnym dátumom.
* Ak clippboard obsahuje text, je možné parametre procesu **vložiť hromadne**. Voľbou "Vložiť parametre" v menu je text rozdelený na jednotlivé slová, pričom každé slovo sa považuje za jeden parameter.
* Skupinu parametrov je možné **skopírovať do clippboardu** ako jeden reťazec.
* **Drag&Drop** umožňuje zapísať cestu súboru do textu parametra.
* **Pozíciu parametrov** je možné vzájomne **vymeniť**  (CTRL+Drag)

## 3. Environmentálne premenné
* Systémové (pre Octopus)
    * Octopus po spustení kontroluje dostupnosť novšej verzie programu na domovskej adrese. Túto adresu je možné zmeniť nastavením systémovej environmentálnej premennej **OCTOPUS_VERSION_CHECK_URL** na alternatívnu adresu pre spustením Octopusa. Ak je zadaná adresa neplatná (napr. "n/a"), kontrola sa nevykoná.
* Lokálne (pre spustený proces)
    * Každému volanému procesu je možné vopred definovať environmentálne premenné, ktoré sa ukladajú pre každú kombináciu [program]-[parametre]. Pracovná zložka sa pritom nezohľadňuje. Vnorené environmentálne premenné sú expandované pomocou shellu.
    * Vybrané riadky (dvojice nazov parametra+hodnota) je možné kopírovať do clipboardu a späť
    * Ak je medzi environmentálnymi premennými procesu definovaná premenná **OCTOPUS_IGNORE_TASKBAR_PROGRESS,** spustenie takéhoto procesu nebude mať vplyv na indikáciu bežiaceho procesu vo windows taskbare.


## 4. Spustenie programu
* Spustenie programu je možné cez menu aplikácie alebo tlačidlom "Spustiť proces". Aplikácia pred spustením procesu testuje existenciu a spustiteľnosť zvoleného súboru. V prípade, že je zadaný len názov súboru, aplikácia sa pokúsi loklizovať súbor v systémových priečinkoch.
* Pracovná zložka je nastavená na prednastavenú hodnotu, ktorú je možné zmeniť v menu cez "Zmeniť prac. zložku".
* Po úspešnom teste je možné spustiť proces v troch režimoch:
    * "Spustiť proces" so zobrazením nemodálneho dialógového okna s výstupom z procesu
    * "Spustiť proces napozadí" bez zobrazenia dialógového okna. Dialóg je možné zobraziť kliknutím na trayicon. Po úspešnom ukončení procesu (návratová hodnota je 0) sa dialógové okno zavrie.
    * "Spustiť proces samostatne" bez možnosti interakcie s ním.

## 5. História volaní
* Po spustení programu sa **prehľadáva história**. Ak už bol program spustený s danými parametreami, jeho **história je presunutá** celkom **hore**. Ak takýto program ešte nebol spustený alebo jeho parametre sa zmenili, je do histórie zapísaný názov programu, jeho parametre a pracovný priečinok.
* Označené položky histórie 
	* je možné **zmazať** cez položku v menu alebo kontextové menu.
	* je možné** exportovať **alebo **importovať** cez položku v menu alebo kontextové menu. 
* Pre označenú položku histórie je možné **otvoriť v súborovom prehliadači**
	* **priečinok spusteného programu**
	* **pracovný priečinok**
* Pre lepšiu orientáciu v histórii je možné **kliknutím do záhlavia tabuľky zadať filter**, podľa ktorého sa obmedzí zobrazenie jednotlivých riadkov. Filter je možné zrušiť zadaním prázdneho reťazca alebo CTRL+ľavý klik myši do háhlavia tabuľky.
* **Vyhľadávanie ľubovoľného textu** v riadku histórii pomocou CTRL+F, pričom nájdený reťazec zvýrazní bunku, kde bol nájdený
* V prípade, že sa názov programu alebo zoznam parametrov nezmestí na šírku stĺpca, je aktivovaný **tooltip s celým názvom**. Ako filter je možné použiť **hodnotu zadanú v poli Program alebo Parametre** pomocou kontextovej ponuky.
* **História príkazov** sa zapisujú do **xml** súboru na lokálny disk.

## 6. Výstup programu
* V okne procesu sa **zobrazuje jeho výstup v dvoch záložkách** oddelene pre štandardný výstup a výstup chybových hlásení.
* **Výstup** procesu **obsahuje kontextovú ponuku**, ktorá ponúka možnosť 
    * **zmeniť font písma**
    * **zmeniť znakovú sadu**
    * **uložiť výstup do súboru** (názov výstupného súboru je predvyplnený ako _[program][datum_cas].[pripona]_, pričom _prípona_ je pre štandardný výstup predvyplnená na _log_, pre výstup chybových hlásení na _err_ a pre HTML výstup na _html_)
    * **reštartovať** proces
* **Vybraná časť **(vybraná počas stlačeného klávesu CTRL) **textu je farebne zvýraznená** v celom výstupe.
* **Vybranú časť **textu je možné **hľadať v texte** dopredne pomocou klávesy F3 a spätne pomocou SHIFT + F3
* **Vyhľadávanie ľubovoľného textu** je možné zadať cez kontextovú ponuku _Hľadaj....CTRL+F_
* **Prepínanie medzi záložkami** pomocou Alt+1 a Alt+2

## 7. Vstup programu
Počas behu programu je možné v okne procesu v spodnej časti zobraziť **textové políčko**, kde **je možné zadať text** a odoslať procesu tlačidlom alebo klávesom ENTER. Po ukončení procesu sa možnosť zadávať text automaticky deaktivuje.

## 8. Nastavenia

**Aplikácia** si **ukladá** nasledovné **parametre**:

* Hlavne okno:
    * Veľkosť okna
    * Pozícia okna
	* Veľkosť stĺpcov v histórii
	* Filtre historie
	* Pracovný priečinok
* Dialóg výstupu procesu:
	* Znaková sada výstupu procesu
	* Font výstupu procesu
	* Veľkosť okna
	* Pozícia okna
	* Pracovný priečinok
* Dialóg environmentálnych premenných
    * Veľkosť okna
	* Veľkosť stĺpcov

Nastavenia sa načítavajú pri štarte aplikácie a ukladajú sa automaticky pri ukončení aplikácie.

## 9. Tray icon
Po spustení procesu sa zobrazí neaktívna Tray ikona. Po ukončení procesu je ikona aktívna a zobrazí sa správa s názovm programu a návratovou hodnotou. Po kliknutí na správu alebo na ikonu sa zobrazí príslušné okno procesu. Pre každé okno sa zobrazí vlastná ikona, ktorá zmizne po zatvorení príslušného dialógového okna.

## 10. Windows Extra
* Počas doby behu procesov sa zobrazuje progres vo windows taskbare. Ak je počet procesov väčší ako jeden, zobrazuje sa aj počet aktívnych procesov.
* Windows thumbnail toolbar obsahuje tlačidlo "Spustiť proces" pre rýchle opätovné spustenie procesu.

## 11. Prepínače aplikácie
Aplikáciu je možné spustiť aj bez načítania histórie alebo načítania uložených parametrov okna. Dostupné parametre je možné zobraziť spustením aplikácie s prepínačom **-h**. Pri ukončení aplikácie sa história a parametre okna ukladajú vždy.


      --ignore-config		Nenačítať uložené nastavnia
      --ignore-history  	Nenačítať históriu
      --language <lang>		Vynútiť zmenu jazyka aplikácie. Dostupné voľby pre lang: "sk" (Slovenčina), inak (English)
      --styles [Name]   	Zobazí zoznam dostupných GUI štýlov

## 12. Zápisník aplikácie
Aplikácia automaticky zapisuje chybové hlásenia do log súboru v príslušnom temp priečinku.
 

## 13. Ukončenie aplikácie
Používateľ môže zatvoriť dialógové okná procesov jednotlivo alebo ukončením hlavného okna aplikácie. V prípade, že je nejaký proces stále aktívny, je na to používateľ upozornený alebo pri pokuse zatvoriť dialógové okno alebo pri pokuse zatvoriť aplikáciu.

_(Posledná aktualizácia: 3.4.2019)_
