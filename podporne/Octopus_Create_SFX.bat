@echo off
::deploy skript na dropbox
::-------------------------------
SET VERZIA=0.12.0(10.1.2017)
::-------------------------------
SET OCT-TMP=%TMP%\Oct-sfx
SET OCT-ZDROJ=%ProgramFiles(x86)%\Octopus
SET OCT-CIEL=C:\Users\vm\Dropbox\_ACE\Octopus-neophyte-win\_install
SET OCT-SFX-NAZOV=Octopus_sfx_%VERZIA%.exe
::-------------------------------

echo _______________________________________________________
echo Nazov:         %OCT-SFX-NAZOV%
echo Zdroj:         %OCT-ZDROJ%
echo Ciel:          %OCT-CIEL%
echo Temporarka:    %OCT-TMP%
echo _______________________________________________________
echo.
pause

if not exist "%OCT-ZDROJ%" (
    echo.
    echo Je potrebne nainstalovat Octopus!
    pause
    goto :eof
)
if not exist "%OCT-CIEL%" (
    echo.
    echo Neexistuje "%OCT-CIEL%" !!!
    pause
    goto :eof
)

echo.
echo *******************************************************
echo Pripravujem TMP (%OCT-TMP%) ...
echo *******************************************************
if exist "%OCT-TMP%" rmdir /Q /S "%OCT-TMP%"

MKDIR "%OCT-TMP%\Octopus"
if errorlevel 1 goto :chybka

Xcopy /Q /E "%OCT-ZDROJ%" "%OCT-TMP%\Octopus"
if errorlevel 1 goto :chybka

pushd "%OCT-TMP%"
del /Q /F Octopus\unins000*
if errorlevel 1 goto :chybka

echo.
echo *******************************************************
echo Vytvaram archiv (%OCT-SFX-NAZOV%)...
echo *******************************************************
"c:\Program Files\7-Zip\7zG.exe" a -r -sfx "%OCT-SFX-NAZOV%" Octopus
if errorlevel 1 goto :chybka

echo.
echo *******************************************************
echo Presuvam do (%OCT-CIEL%) ...
echo *******************************************************
move "%OCT-SFX-NAZOV%" "%OCT-CIEL%"
if errorlevel 1 goto :chybka

echo.
echo *******************************************************
echo Cistenie ...
echo *******************************************************
popd
if exist "%OCT-TMP%" rmdir /Q /S "%OCT-TMP%"

timeout 10
goto :eof

:chybka
echo.
echo.
echo neocakavana chyba!!!
echo.
echo.
pause > nul
