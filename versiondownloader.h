/**
 ** This file is part of the Octopus project.
 ** Copyright 2023 wahibre <wahibre@gmx.com>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#ifndef VERSIONDOWNLOADER_H
#define VERSIONDOWNLOADER_H

#include <QUrl>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QThread>

#define VERSION_CHECK_URL       "https://gitlab.com/wahibre/octopus/raw/master/changelog.txt"
#define VERSION_DOWNLOAD        "https://gitlab.com/wahibre/octopus/releases"

class VersionDownloader : public QObject
{
    Q_OBJECT

private:
    QUrl verzieUrl;
    QNetworkAccessManager *manager;
    QString changelogText;
    bool beziVoVlastnomVlakne;

private slots:
    void spracujOdpoved(QNetworkReply *reply);
    void spracujChybu(QNetworkReply::NetworkError code);
    void spracujSSLchyby(const QList<QSslError> &errors);
    void zistiDostupneZmeny();

public:
    explicit VersionDownloader();
    static QString downloadURL() {return QString(VERSION_DOWNLOAD).toHtmlEscaped();}
    bool getBeziVoVlastnomVlakne() const;

signals:
    void zmenyNacitane(const QString &zmney, const bool existujeNovsiaVerzia);
};

#endif // VERSIONDOWNLOADER_H
