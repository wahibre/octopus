/**
 ** This file is part of the Octopus project.
 ** Copyright 2023 wahibre <wahibre@gmx.com>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include<QApplication>
#include <QStandardPaths>
#include<QFileInfo>
#include<QDir>
#include "logovanie.h"

Logovanie::Logovanie(QFile *subor)
{
    QDir TmpPriecinok; TmpPriecinok.setPath(QStandardPaths::writableLocation(QStandardPaths::TempLocation));
    QString nazovsuboru = QApplication::applicationName();

    logfile = subor;

    QFileInfo *loginfo = new QFileInfo
    (
        TmpPriecinok,
        nazovsuboru+".log"
    );

    subor->setFileName(loginfo->absoluteFilePath());

    if(subor->open(QIODevice::Append))
    {
        logstream.setDevice(subor);
    }
    delete loginfo;
}
Logovanie::~Logovanie()
{
    if(logfile->isOpen())
        logfile->close();
    qInstallMessageHandler(0);
}

bool Logovanie::IsActive()
{
    return logfile->isOpen();
}
