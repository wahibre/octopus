/**
 ** This file is part of the Octopus project.
 ** Copyright 2023 wahibre <wahibre@gmx.com>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include<QApplication>
#include<QFile>
#include<QDebug>
#include<QtGlobal>
#include<QDateTime>
#include<QCommandLineParser>
#include <QStyleFactory>
#include "winmain.h"
#include "logovanie.h"

QFile logfile;
QTextStream logstream;

#define IGNORE_CONFIG  "ignore-config"
#define IGNORE_HISTORY "ignore-history"
#define LIST_STYLES    "styles"

void myMessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    QByteArray localMsg = msg.toLocal8Bit();
    QString preffix, cas;

/* windows bez SSL kniznice zbytocne reportuje warning. SSL sa tu nepoužíva.
    [Warning] : QSslSocket: cannot resolve TLSv1_1_client_method (ssl\qsslsocket_openssl_symbols.cpp:126, void {anonymous}::qsslSocketCannotResolveSymbolWarning(const char*))
    [Warning] : QSslSocket: cannot resolve TLSv1_2_client_method (ssl\qsslsocket_openssl_symbols.cpp:126, void {anonymous}::qsslSocketCannotResolveSymbolWarning(const char*))
    [Warning] : QSslSocket: cannot resolve TLSv1_1_server_method (ssl\qsslsocket_openssl_symbols.cpp:126, void {anonymous}::qsslSocketCannotResolveSymbolWarning(const char*))
    [Warning] : QSslSocket: cannot resolve TLSv1_2_server_method (ssl\qsslsocket_openssl_symbols.cpp:126, void {anonymous}::qsslSocketCannotResolveSymbolWarning(const char*))
    [Warning] : QSslSocket: cannot resolve SSL_select_next_proto (ssl\qsslsocket_openssl_symbols.cpp:126, void {anonymous}::qsslSocketCannotResolveSymbolWarning(const char*))
    [Warning] : QSslSocket: cannot resolve SSL_CTX_set_next_proto_select_cb (ssl\qsslsocket_openssl_symbols.cpp:126, void {anonymous}::qsslSocketCannotResolveSymbolWarning(const char*))
    [Warning] : QSslSocket: cannot resolve SSL_get0_next_proto_negotiated (ssl\qsslsocket_openssl_symbols.cpp:126, void {anonymous}::qsslSocketCannotResolveSymbolWarning(const char*))
*/
    if(type==QtWarningMsg && msg.startsWith("QSslSocket: cannot resolve", Qt::CaseInsensitive))
        return;

    switch (type) {
    case QtDebugMsg:
        preffix = "[Debug]    ";        //nemalo by nastat v release
        break;
/*    case QtInfoMsg:
        preffix = "[Info]    ";
        break;      */
    case QtWarningMsg:
        preffix = "[Warning] ";
        break;
    case QtCriticalMsg:
        preffix = "[Critical]";
        break;
    case QtFatalMsg:
        preffix = "[Fatal]   ";
        break;
    default:
        preffix = QString("[%1]").arg(type);
    }
    cas = QDateTime::currentDateTime().toString("dd.MM.yyyy hh:mm:ss");
    logstream << QString("%1 %2: %3 (%4:%5, %6)\n").arg(cas).arg(preffix).arg(localMsg.constData()).arg(context.file).arg(context.line).arg(context.function);

    if(type == QtFatalMsg)
    {
        logfile.close();
        abort();
    }
}


int main(int argc, char *argv[])
{
    Q_INIT_RESOURCE(resources);

    /* override default style
    if(QStyleFactory::keys().contains("Fusion"))
        QApplication::setStyle(QStyleFactory::create("Fusion"));
    else
        qWarning() << QString("Fusion style not recognized. Valid styles are: %1").arg(QStyleFactory::keys().join(", "));
    */
    QApplication a(argc, argv);
    a.setOrganizationName("Rusty Pipe");
    a.setApplicationName("Octopus");
    a.setApplicationVersion(QString("%1").arg(VERZIA));
    a.setWindowIcon(QIcon(":octopus"));

    QCommandLineParser parser;
    parser.setApplicationDescription("Application for managing multi-switched commands and monitoring output.");
    parser.addHelpOption();
    parser.addVersionOption();

    QCommandLineOption oIgnConf(QString(IGNORE_CONFIG), "Skip reading config file");
    parser.addOption(oIgnConf);

    QCommandLineOption oIgnHist(QString(IGNORE_HISTORY), "Skip reading history file");
    parser.addOption(oIgnHist);

    QCommandLineOption oStyles(QString(LIST_STYLES), "List available GUI styles");
    parser.addOption(oStyles);

    QCommandLineOption oLang("language", "Aplication language (Slovak - sk, English - otherwise)", "lang");
    parser.addOption(oLang);

#if (defined Q_OS_WIN) && QT_VERSION_CHECK(5, 9, 0)
    /* windows nema pripojenu konzolu, nie je kam vypisovat, preto zobrazim cez message box */
    if(argc == 2)
    {
            if(qstrcmp(argv[1],"-h")==0 || qstrcmp(argv[1], "--help")==0 || qstrcmp(argv[1], "/?")==0)
                QMessageBox::information(0, "Help", parser.helpText());
            if(qstrcmp(argv[1],"-v")==0 || qstrcmp(argv[1], "--version")==0)
                QMessageBox::information(0, "Verzia", QString("%1 %2").arg(qAppName()).arg(VERZIA));
    }
#endif
    parser.process(a);

//    presmerovanie qDebug do logu
#ifndef QT_DEBUG
    logstream.setDevice(&logfile);
    Logovanie log(&logfile);
    if(log.IsActive())
        qInstallMessageHandler(myMessageOutput);
    else
        qWarning()<<"Nie je mozne presmerovat output do tmp suboru " << logfile.fileName();
#endif

    if(parser.isSet(LIST_STYLES))
    {
        QMessageBox::information(0, "List of styles", QString("Available GUI styles: %1.\nTo change GUI style use command option: -style [Name]").arg(QStyleFactory::keys().join(", ")));
        return 0;
    }

    auto trans = new QTranslator();

    QString lang;
    if(parser.isSet("language"))
        lang = parser.value("language");
    else
        lang = QLocale::system().name();

    if(lang.left(2).toLower() != "sk")
    {
        if(trans->load(":languages/en"))
            a.installTranslator(trans);
    }



//    spustenie hlavneho okna
    winMain w;
    if(!parser.isSet(IGNORE_CONFIG))
        w.NastaveniaNacitaj();
    if(!parser.isSet(IGNORE_HISTORY))
        w.HistoriaCitaj();
    w.show();

    return a.exec();
}
