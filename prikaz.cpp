/**
 ** This file is part of the Octopus project.
 ** Copyright 2023 wahibre <wahibre@gmx.com>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include "prikaz.h"
#include "winmain.h"
#include <QDomElement>
#include <QDomNode>
#include <QCryptographicHash>

/******************************************************************************************/
Prikaz::Prikaz()
{

}
/******************************************************************************************/
Prikaz::Prikaz(QString program, QStringList parametre, ENV environment, QString pracovnyPriecinok)
{
    this->program = program;
    this->parametre = parametre;
    this->environmenty = environment;
    this->pracovnyPriecinok = pracovnyPriecinok;
    this->hashuj();
}
/******************************************************************************************/
QString Prikaz::dajProgram()
{
    return program;
}
/******************************************************************************************/
QString Prikaz::dajPracovnyPriecinok()
{
    return pracovnyPriecinok;
}
/******************************************************************************************/
QStringList Prikaz::dajParametre()
{
    return parametre;
}
/******************************************************************************************/
QStringList Prikaz::dajParametreEscaped()
{
    QStringList escaped;

    foreach(auto s, parametre)
    {
        if(s.contains(" "))
            escaped << QString("\"%1\"").arg(s);
        else
            escaped << s;
    }
    return escaped;
}
/******************************************************************************************/
ENV Prikaz::dajEnvironmenty()
{
    return environmenty;
}
/******************************************************************************************/
void Prikaz::majPracovnyPriecinok(const QString priecinok)
{
    pracovnyPriecinok = priecinok;
}
/******************************************************************************************/
void Prikaz::majProgramParametre(const QString program, const QStringList parametre)
{
    this->program = program;
    this->parametre = parametre;
    this->hashuj();
}
/******************************************************************************************/
void Prikaz::majEnvironmenty(const ENV environmenty)
{
    this->environmenty = environmenty;
}
/******************************************************************************************/
bool Prikaz::porovnaj(Prikaz p)
{
    return _hash == p._hash;
}
/******************************************************************************************/
QByteArray Prikaz::hash()
{
    return _hash;
}
/******************************************************************************************/
void Prikaz::hashuj()
{
    _hash = QCryptographicHash::hash((program+parametre.join('#')).toUtf8(), QCryptographicHash::Sha1);
}
/******************************************************************************************/
Prikaz Prikaz::fromDomElement(QDomElement element)
{
    Prikaz prikaz;
    QStringList paramsList;
    ENV envList;
    int i;

    QDomNodeList parametre = element.elementsByTagName(HIST_ELEM_PARAMETER);
    QDomNodeList envy = element.elementsByTagName(HIST_ELEM_ENVIRONMENT);

    for (i=0; i < parametre.length(); i++)
        paramsList << parametre.at(i).toElement().text();

    for (i=0; i < envy.length(); i++)
        envList[envy.at(i).toElement().attribute(HIST_ATTR_NAZOV)] = envy.at(i).toElement().text();

    prikaz.majProgramParametre(element.attribute(HIST_ELEM_PROGRAM), paramsList);
    prikaz.majPracovnyPriecinok(element.attribute(HIST_ELEM_WORKDIR));
    prikaz.majEnvironmenty(envList);

    return prikaz;
}
/******************************************************************************************/
QString Prikaz::toScript()
{
    /* windows style only so far */
    QString script;

    script += QString("@echo off\n\n");

    QHashIterator<QString, QString> i(environmenty);
    while(i.hasNext())
    {
        i.next();
        script += QString("SET \"%1=%2\"\n").arg(i.key()).arg(i.value());
    }
    if(environmenty.size() > 0)
        script += QString("\n");
    script += QString("cd /D \"%1\"\n").arg(pracovnyPriecinok);
    script += QString("\"%1\" %2\n").arg(program).arg(dajParametreEscaped().join(" "));

    return script;
}
/******************************************************************************************/
