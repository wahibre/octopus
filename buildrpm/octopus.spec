%global     debug_package %nil
Name:		octopus
Version: 	1.0
Release:	1%{?dist}
Summary:	Octopus

Group:		Amusements/Graphics
License:	GPLv3
URL:		http://gitlab.com/wahibre/octopus/wikis/
Source0:	https://gitlab.com/wahibre/%name/-/archive/%version/%name-%version.tar.gz

BuildRequires:	gcc-c++ qt5-qtbase-devel qt5-qtxmlpatterns-devel
Requires:	qt5-qtbase qt5-qtxmlpatterns qt5-qtsvg

%description
Octopus

%prep
%setup

%build
qmake-qt5 INSTALL_ROOT=%{buildroot} Octopus.pro
%make_build

%install
%make_install INSTALL_ROOT=%{buildroot}

%files
%{_bindir}/octopus
%{_datadir}/applications/%{name}.desktop
%{_datadir}/pixmaps/%{name}.png

%changelog
* Tue Dec 20 2022 wahibre  <wahibre@gmx.com> - 1.0
- udpate to version 1.0

* Thu Jul 26 2018 wahibre  <wahibre@gmx.com> - 0.1
[*] changelog is now reporting changes in spec file
