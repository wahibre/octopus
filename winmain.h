/**
 ** This file is part of the Octopus project.
 ** Copyright 2023 wahibre <wahibre@gmx.com>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#ifndef WINMAIN_H
#define WINMAIN_H

#include <QMainWindow>
#include <QCompleter>
#include <QTableWidget>
#include <QSettings>
#include <QtXml/QtXml>
#include <QtGlobal>
#include <QMessageBox>

#include "winprocess.h"
#include "processwatcher.h"
#include "historiamodel.h"
#include "historiaproxymodel.h"
#include "versiondownloader.h"
#include "findtextwidget.h"

namespace Ui {
class winMain;
}

class winMain : public QMainWindow
{
    Q_OBJECT

    const QString L_WINT_ZMENY_APP    = tr("Dostupné zmeny aplikácie");
    const QString L_MENU_ZMENY_APP    = QString("==> %1 <==").arg(tr("&Dostupná nová verzia"));
    const QString L_ZMENA_PRAC_ZLOZKY = tr("Zmeniť pracovnú zložku");

public:
    explicit winMain(QWidget *parent = Q_NULLPTR);
    ~winMain() Q_DECL_OVERRIDE;

    void HistoriaZapis();
    void HistoriaCitaj();
    void NastaveniaZapis();
    void NastaveniaNacitaj();

private slots:
    void closeEvent(QCloseEvent *event) Q_DECL_OVERRIDE;
    void ClipBoardDataChanged();
    void historiaHlavickaClicked(int idx);
    void najdenaNovsiaVeria(const QString &text, const bool existujeNovsiaVerzia);
    void zobrazZmenyDlg(bool);
    void RunProcessWin();
    void aktualizujEnvButtonStatus();
    void onFiltrujProgram(QString vzor);
    void onFiltrujParametre(QString vzor);
    void onVymenaLineEdit(int indexZdroj, int indexCiel);
    void hladajHistoria(QString text, bool caseSensitive, bool backward);
    void updateHistoriaViewPort(QString text, bool caseSensitive);

    void on_btnOpenProgram_clicked();
    void on_btnRun_clicked();
    void on_btnAddParameter_clicked();
    void on_btnRemParameter_clicked();
    void on_tableView_doubleClicked(const QModelIndex &index);
    void on_actionDelProgram_triggered();
    void on_actionSpusti_proces_triggered();
    void on_actionSpustiProcesPotichucky_triggered();
    void on_actionSpustiProcesOdpojene_triggered();
    void on_actionAboutQt_triggered();
    void on_action_Ukoncit_triggered();
    void on_actionPridat_parametre_triggered();
    void on_action_Odstranitparametre_triggered();
    void on_actionAbout_triggered();
    void on_actionKopirovatParametre_triggered();
    void on_action_VlozitParametre_triggered();
    void on_actionZmenitPracZlozku_triggered();
    void on_actionExportVybranych_triggered();
    void on_actionImportHistoria_triggered();
    void on_actionOtvor_prac_priecinok_triggered();
    void on_actionLokalizuj_program_triggered();
    void on_actionEnvironment_triggered();
    void on_actionExportAsScript_triggered();
    void on_actionHladaj_triggered();

private:
    Ui::winMain *ui;
    ProcessWatcher processWatcher;
    QTimer *budik;
    HistoriaModel historiaModel;
    HistoriaProxyModel historiaFilterModel;
    QCompleter *nasepkavacProg;
    QCompleter *nasepkavacParam;
    Prikaz prikaz;
    QString changelog;
    QPointer<VersionDownloader> versionDownloader;
    FindTextWidget* finder;
#ifdef Q_OS_WIN
    QWinThumbnailToolBar *thumbnailToolBar;
#endif
    void AddItemToHistory(Prikaz prikaz);
    void RemoveSelectedItemsFromHistory();
    void AddParams();
    void AddParamsFromList(QStringList parametre);
    QStringList CreateParamsList();
    Prikaz &aktualizujPrikaz();
    void ParseParamsFromClipboard();
    void CopyParamsToClipboard();
    void RemParams(bool allWidgets);
    void DoplnNasepkavacParametrov(QStringList noveParametre);
    virtual void dropEvent(QDropEvent * event) Q_DECL_OVERRIDE;
    virtual void dragEnterEvent(QDragEnterEvent * event) Q_DECL_OVERRIDE;
    virtual void showEvent(QShowEvent *) Q_DECL_OVERRIDE;
    virtual void keyPressEvent(QKeyEvent *event) Q_DECL_OVERRIDE;
    virtual void keyReleaseEvent(QKeyEvent *event) Q_DECL_OVERRIDE;
    void filtruj(int idx, QString filterTxt);
    void NastavPracZlozku(QString zlozka);
    void HistoriaViewExportSelected();
    void HistoriaImport();
    void OtvorPriecinok(QModelIndex selIdx, QString openDirFrom);
    void vytvorThumbnailExecButton();
    void RunProcess(StartProcesu start);
    void updateRunButtonProperty(QString mode=QString());
};

#endif // WINMAIN_H
