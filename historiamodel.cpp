/**
 ** This file is part of the Octopus project.
 ** Copyright 2023 wahibre <wahibre@gmx.com>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include "historiamodel.h"
#include "konstanty.h"

#include <QDir>
#include <QDomNode>
#include <QFileInfo>
#include <QStandardPaths>
#include <QTableWidget>
#include <QDebug>
#include <QApplication>

/******************************************************************************************/
HistoriaModel::HistoriaModel(QObject *parent):
    QAbstractItemModel(parent)
{
    dataLocation = QStandardPaths::writableLocation(QStandardPaths::DataLocation);

    HistHeadersNames[0] = tr("Program");
    HistHeadersNames[1] = tr("Parametre");
}
/******************************************************************************************/
bool HistoriaModel::loadData()
{
    QFile subor;
    QDir dataDir(dataLocation);
    QFileInfo fi;
    QString errorStr;
    QDomNodeList elements;
    int errorLine;
    int errorColumn;

    fi.setFile(dataDir, CONFIGSUBOR);
    subor.setFileName(fi.absoluteFilePath());

    if(!subor.exists())
        return false;
    if(subor.bytesAvailable() == 0)
        return false;

    if (!DomHistory.setContent(&subor, true, &errorStr, &errorLine, &errorColumn))
    {
        qCritical()<< "Parse error at line" << errorLine <<", column "<< errorColumn <<": "<< errorStr;
        return false;
    }
    elements = DomHistory.elementsByTagName(HIST_ELEM_PRIKAZ);

    beginInsertRows(QModelIndex(), 0, elements.length()-1);
    prikazy = elements;
    endInsertRows();
    return prikazy.length() > 0;
}
/******************************************************************************************/
void HistoriaModel::saveData()
{
    QFile subor;
    QDir dataDir(dataLocation);
    QFileInfo fi;

    if (!dataDir.exists())
        if(!dataDir.mkpath("."))
            qWarning()<<tr("Nie je mozne vytvorit priecinok")<< dataLocation;

    fi.setFile(dataDir, CONFIGSUBOR);
    subor.setFileName(fi.absoluteFilePath());

    if(subor.open(QIODevice::WriteOnly|QIODevice::Text))
    {
        QTextStream ts(&subor);
        DomHistory.save(ts, HIST_INDENT/*, QDomNode::EncodingFromTextStream*/);
        subor.close();
    }
    else
        qCritical()<<"Nie je možné zapisovať do súboru "<< subor.fileName();
}
/******************************************************************************************/
QString HistoriaModel::getFullTextSearch() const
{
    return fullTextSearch;
}
/******************************************************************************************/
void HistoriaModel::setFullTextSearch(const QString &value, Qt::CaseSensitivity sensitivity)
{
    fullTextSearch = value;
    fullTextSearchSensitivity = sensitivity;
}
/******************************************************************************************/
QStringList HistoriaModel::vsetkyParametreHistorie()
{
    QStringList paramsList;
    QDomNodeList parametre;
    QDomElement prikazElement;

    for(int row=0; row<prikazy.length(); row++)
    {
        prikazElement = prikazy.at(row).toElement();

        parametre = prikazElement.elementsByTagName(HIST_ELEM_PARAMETER);
        for (int i=0;i< parametre.count(); i++)
            paramsList << parametre.at(i).toElement().text();
    }
    return paramsList;
}
/******************************************************************************************/
Prikaz HistoriaModel::dajPrikaz(int row)
{
    ENV e;
    QStringList paramsList;
    QDomNodeList parametre;
    QDomElement prikazElement;
    QDomNodeList environmenty;
    Prikaz p;

    prikazElement = prikazy.at(row).toElement();

    /* parametre */
    parametre = prikazElement.elementsByTagName(HIST_ELEM_PARAMETER);
    for (int i=0;i< parametre.count(); i++)
        paramsList << parametre.at(i).toElement().text();

    /* environmentalne premenne */
    environmenty = prikazElement.elementsByTagName(HIST_ELEM_ENVIRONMENT);

    for (int i=0; i<environmenty.length(); i++)
        e[environmenty.at(i).toElement().attribute(HIST_ATTR_NAZOV)] = environmenty.at(i).toElement().text();


    p.majProgramParametre(prikazElement.attribute(HIST_ELEM_PROGRAM), paramsList);
    p.majPracovnyPriecinok(prikazElement.attribute(HIST_ELEM_WORKDIR));
    p.majEnvironmenty(e);

    return p;
}
/******************************************************************************************/
int HistoriaModel::pocet()
{
    return prikazy.count();
}
/******************************************************************************************/
QString HistoriaModel::dajProgram(const QModelIndex &idx) const
{
    if(idx.isValid())
        return data(index(idx.row(), 0, QModelIndex()), Qt::DisplayRole).toString();

    return QString();
}
/******************************************************************************************/
QString HistoriaModel::dajParams(const QModelIndex &idx) const
{
    if(idx.isValid())
        return data(index(idx.row(), 1, QModelIndex()), Qt::DisplayRole).toString();

    return QString();
}
/******************************************************************************************/
bool HistoriaModel::existujeTextVProgram(const QModelIndex &idx, QString text, Qt::CaseSensitivity caseSensitivity) const
{
    return dajProgram(idx).contains(text, caseSensitivity) ? true : false;
}
/******************************************************************************************/
bool HistoriaModel::existujeTextVParams(const QModelIndex &idx, QString text, Qt::CaseSensitivity caseSensitivity) const
{
    return dajParams(idx).contains(text, caseSensitivity) ? true : false;
}
/******************************************************************************************/
bool HistoriaModel::existujeTextVRiadku(const QModelIndex &idx, QString text, Qt::CaseSensitivity caseSensitivity) const
{
    if(existujeTextVProgram(idx, text, caseSensitivity))
        return true;

    if(existujeTextVParams(idx, text, caseSensitivity))
        return true;

    return false;
}
/******************************************************************************************/
QDomDocument HistoriaModel::vytvorDOM(QModelIndexList vybrane, int &pocetExportovanych)
{
    QDomDocument dokExport;
    QDomElement koren;
    int pocet = 0;

    /* hlavny uzol xml dokumentu */
    koren = dokExport.createElement(HIST_ELEM_HISTORIA);
    dokExport.appendChild(koren);

    foreach(auto i, vybrane)
    {
        if(i.column() == 0)
        {
            QDomNode n = koren.appendChild(prikazy.at(i.row()).cloneNode(true));
            if(n.isNull())
                break;
            pocet++;
        }
    }
    pocetExportovanych = pocet;
    return dokExport;
}
/******************************************************************************************/
void HistoriaModel::zmazRiadky(QModelIndexList vybrane)
{
    QList<int> zoznamNaZmazanie;

    foreach(auto i, vybrane)
    {
        if(i.column()==0)
            zoznamNaZmazanie.append(i.row());
    }

    std::sort(zoznamNaZmazanie.begin(), zoznamNaZmazanie.end(), std::greater<int>());

    foreach(auto i, zoznamNaZmazanie)
        removeRows(i, 1, QModelIndex());
}
/******************************************************************************************/
void HistoriaModel::zmazPrikaz(Prikaz prikaz)
{
    for(int i=prikazy.length()-1; i>=0; i--)
    {
        if(dajPrikaz(i).porovnaj(prikaz))
            removeRows(i, 1, QModelIndex());
    }
}
/******************************************************************************************/
void HistoriaModel::vlozPrikaz(Prikaz prikaz)
{
    QDomElement novyPrikaz = DomHistory.createElement(HIST_ELEM_PRIKAZ);
    novyPrikaz.setAttribute(HIST_ELEM_PROGRAM, prikaz.dajProgram());
    novyPrikaz.setAttribute(HIST_ELEM_WORKDIR, prikaz.dajPracovnyPriecinok());

    foreach (QString s, prikaz.dajParametre())
    {
        QDomElement paramElem = DomHistory.createElement(HIST_ELEM_PARAMETER);
        paramElem.appendChild(DomHistory.createTextNode(s));
        novyPrikaz.appendChild(paramElem);
    }

    ENV envky = prikaz.dajEnvironmenty();
    for(auto i = envky.begin(); i != envky.end(); i++)
    {
        QDomElement envElem = DomHistory.createElement(HIST_ELEM_ENVIRONMENT);
        envElem.appendChild(DomHistory.createTextNode(i.value()));
        envElem.setAttribute(HIST_ATTR_NAZOV, i.key());
        novyPrikaz.appendChild(envElem);
    }

    QDomElement elHistoria = DomHistory.firstChildElement(HIST_ELEM_HISTORIA);
    if (elHistoria.isNull())
        elHistoria = DomHistory.createElement(HIST_ELEM_HISTORIA);

    QDomElement prvyPrikaz = elHistoria.firstChildElement();
    elHistoria.insertBefore(novyPrikaz, prvyPrikaz);

    beginInsertRows(QModelIndex(), 0, 0);
    DomHistory.insertBefore(elHistoria, prvyPrikaz);
    endInsertRows();
}
/******************************************************************************************/
QModelIndex HistoriaModel::index(int row, int column, const QModelIndex &parent) const
{
    Q_UNUSED(parent)

    return createIndex(row, column);
}
/******************************************************************************************/
QModelIndex HistoriaModel::parent(const QModelIndex &child) const
{
    Q_UNUSED(child)
    return QModelIndex();
}
/******************************************************************************************/
int HistoriaModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return prikazy.count();
}
/******************************************************************************************/
int HistoriaModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return 2;
}
/******************************************************************************************/
QVariant HistoriaModel::data(const QModelIndex &index, int role) const
{
    if (role == Qt::DisplayRole)
    {
        if(index.column() == 0)
            return prikazy.at(index.row()).attributes().namedItem(HIST_ELEM_PROGRAM).nodeValue();

        if(index.column() == 1)
        {
            QStringList slPar;
            QString param;

            QDomNodeList parametre = prikazy.at(index.row()).toElement().elementsByTagName(HIST_ELEM_PARAMETER);
            for(int j=0; j<parametre.count(); j++)
            {
                if (parametre.at(j).hasChildNodes())
                {
                    param = parametre.at(j).firstChild().nodeValue().trimmed();
                    if(!param.isEmpty())
                        slPar << param;
                }
            }
            return slPar.join(' ');
        }
    }
    else
#if (QT_VERSION > QT_VERSION_CHECK(5, 13, 0))
        if(role == Qt::BackgroundRole)
#else
        if(role == Qt::BackgroundColorRole)
#endif
        {
            if(!fullTextSearch.isEmpty())
            {
                if(
                    (index.column() == 0 && existujeTextVProgram(index, fullTextSearch, fullTextSearchSensitivity))
                    ||
                    (index.column() == 1 && existujeTextVParams( index, fullTextSearch, fullTextSearchSensitivity))
                )
                    return QColor(HIST_COLOR_SEARCH);
            }
        }

    return QVariant();
}
/******************************************************************************************/
QVariant HistoriaModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(orientation == Qt::Horizontal)
    {
        if(role == Qt::DisplayRole)
            return HistHeadersNames[section];
    }
    return QAbstractItemModel::headerData(section, orientation, role);
}
 /******************************************************************************************/
 bool HistoriaModel::removeRows(int row, int count, const QModelIndex &parent)
 {
     Q_UNUSED(parent)

     if(count == 1)
     {
         QDomNode mazak;
         beginRemoveRows(QModelIndex(), row, row);
         mazak = prikazy.at(row);
         mazak.parentNode().removeChild(mazak);
         endRemoveRows();
         return true;
    }
     else
         qWarning() << tr("Mazanie viacero riadkov v modeli súčasne nie je podporované!");

     return false;
 }
 /******************************************************************************************/
