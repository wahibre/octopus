/**
 ** This file is part of the Octopus project.
 ** Copyright 2023 wahibre <wahibre@gmx.com>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#ifndef MYTEXTBROWSER_H
#define MYTEXTBROWSER_H

#include <QTextBrowser>
#include <QWidget>
#include <QLineEdit>
#include <QVBoxLayout>
#include <QFrame>
#include <QStateMachine>
#include <QGraphicsOpacityEffect>
#include <QMenu>
#include "findtextwidget.h"

class MyTextBrowser : public QTextBrowser
{
    Q_OBJECT
public:
    const QString L_HLADAJ      = tr("Hľadaj...");
    const QString L_ZALAMOVANIE = tr("Zalamovanie");

    explicit MyTextBrowser(QWidget *parent=0);
    void hladacikZobrazit(bool);
    QMenu *createStandardContextMenu(const QPoint &pos);
    QAction *akciaHladaj;
    QAction *akciaZalamovanie;
private slots:
    void toggleLineWrap(bool checked);
    void vyhladaj(QString text, bool caseSensitive, bool backward);
private:
    FindTextWidget *finder;
    void keyPressEvent(QKeyEvent * ev);
    bool event(QEvent *e)                           Q_DECL_OVERRIDE;
};

#endif // MYTEXTBROWSER_H
