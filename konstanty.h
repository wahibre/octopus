/**
 ** This file is part of the Octopus project.
 ** Copyright 2023 wahibre <wahibre@gmx.com>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#ifndef KONSTANTY_H
#define KONSTANTY_H

/* farebne zvyraznenie aktivnej zalozky */
#define TAB_COLOR_OUT Qt::darkGreen
#define TAB_COLOR_ERR Qt::red
#define TAB_COLOR_SEL Qt::yellow

/* farba fulltext hladania */
#define HIST_COLOR_SEARCH "#88FFFF"

/* doba animacie zadavania vstupu */
#define DOBA_ANIMACIE 300

/* nazov XML suboru historie spustenych programov */
#define CONFIGSUBOR "historia.xml"
#define HIST_INDENT 4

/* nastavenia/registre */
#define REG_COMPLETER_MODE "NasepkavaciMod"
#define REG_WORK_DIRECTORY "WorkDirectory"
#define REG_FILTER_0 "ProgramFilter"
#define REG_FILTER_1 "ParamFilter"
#define REG_WIDTH_COL_0 "Stlpec1_sirka"

/* Oneskorenie kontroly novej verzie */
#define ONESKORENIE_KONTROLY_VERZIE_MS 3000

/* vyuzivanie environmentalnych premennych */
#define IGNORE_TASKBAR_PROGRESS_ENV "OCTOPUS_IGNORE_TASKBAR_PROGRESS"
#define VERSION_CHECK_URL_ENV       "OCTOPUS_VERSION_CHECK_URL"

#endif // KONSTANTY_H

