/**
 ** This file is part of the Octopus project.
 ** Copyright 2023 wahibre <wahibre@gmx.com>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#ifndef ZVYRAZNOVAC_H
#define ZVYRAZNOVAC_H

#include <QObject>
#include <QTextEdit>
#include <QMutex>
#include <QTimer>
#include <QTime>

#define ZVYRAZ_ONESKORENIE 1000

class Zvyraznovac : public QObject
{
    Q_OBJECT
public:
    explicit Zvyraznovac(QTextEdit *textEditor, const QColor inFarbaPozadia, QObject *parent=0);
    void zvyrazniVybrane();

private slots:
    void zvyrazniText();

private:
    QTextEdit*  editor;
    QColor      farbaPozadia;
    QMutex      mutex;
    QTextCursor kurzorVyberu;
    QTimer      budik;

};

#endif // ZVYRAZNOVAC_H
