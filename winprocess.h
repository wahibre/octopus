/**
 ** This file is part of the Octopus project.
 ** Copyright 2023 wahibre <wahibre@gmx.com>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#ifndef DLGOUTPUT_H
#define DLGOUTPUT_H

#include <QDialog>
#include <QProcess>
#include <QMutex>
#include <QSystemTrayIcon>
#include <QDateTime>
#include <QList>
#include <QTextCodec>
#include <QTextBrowser>
#include <QStateMachine>
#include <QElapsedTimer>

#include "zvyraznovac.h"
#include "konstanty.h"
#include "mytextbrowser.h"
#include "prikaz.h"

#define TAB_STD_OUT 0
#define TAB_STD_ERR 1
#define _O_K_ 0

#ifdef Q_OS_WIN
#define ENVIRON_CHAR '%'
#else
#define ENVIRON_CHAR '$'
#endif
namespace Ui {
class winProcess;
}

enum StartProcesu
{
    beziaci_vpredu=1,
    beziaci_vzadu=2,
    beziaci_mimo=3
};

class winProcess : public QDialog
{
    Q_OBJECT

public:
    winProcess(Prikaz prikaz, StartProcesu startProcesu, QWidget *parent=Q_NULLPTR);
    ~winProcess()           Q_DECL_OVERRIDE;
    bool jeProcesAktivny();
    QString nazovProgramu();
    void runProcess();
private:
    Ui::winProcess  *ui;
    QProcess        *proc;
    QString         program;
    QString         processName;
    QStringList     parametre;
    QString         workdir;
    QString         programHash;
    QString         statusMsg;
    QByteArray      outputStdOutText;
    QByteArray      outputStdErrText;
    QMutex          readingProcess;
    QSystemTrayIcon TrayIcon;
    QIcon           TrayOnIcon;
    QIcon           TrayOffIcon;
    QStringList     dostupneKodeky;
    QTextCodec      *mojTextKodek;
    QDateTime       casSpustenia;
    QDateTime       casUkoncenia;
    QElapsedTimer   dobaBehu;
    QElapsedTimer   duration;
    int             returnCode;
    StartProcesu    startProcesu;
    QStateMachine   *automat;
    QTimer          *tmrDobaBehu;
    Zvyraznovac     *zvyrazniVybraneIO;
    Zvyraznovac     *zvyrazniVybraneErr;
    ENV             environmenty;
    QTextCursor     *cur_stdout_at_end;
    QTextCursor     *cur_stderr_at_end;

    void reject()                           Q_DECL_OVERRIDE;
    void keyReleaseEvent(QKeyEvent * ev)    Q_DECL_OVERRIDE;
    void NastaveniaZapis();
    void NastaveniaCitaj();
    void nacitajDostupneKodeky();
    void SaveLogFile(QByteArray outText, QString fExtension);
    void MyContextMenu(MyTextBrowser *browser, QAction *saveAction, QPoint bod);
    void nastavAutomat();
    QString htmlTableRow(QString name, QString value);
    QString dateTimeFileName();
    QString encodeCssFont(const QFont &refFont);
    QString GetStdoutFromCommand(QString cmd);
    void killProcess();
    void initProcess();

private slots:
    void Started();
    void Citanie();
    void CitanieErr();
    void Zapisovanie();
    void ObnovDobuBehu();
    void Ukoncenie(int vysledok);
    void SaveStdOutlogfile();
    void SaveStdErrlogfile();
    bool ZmenKodovanie(QString novyNazovKodeku = QString());
    void ZmenFont();
    void ZobrazOkno();
    void ExportHtml();
    void Restart();
    void MyContextMenuStdOut(QPoint bod);
    void MyContextMenuStdErr(QPoint bod);
    void on_tabWidget_currentChanged(int index);
    void on_textEdit_selectionChanged();
    void on_textBrowserErr_selectionChanged();
signals:
    void processStarted();
    void processFinished();
};

#endif // DLGOUTPUT_H
