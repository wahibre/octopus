/**
 ** This file is part of the Octopus project.
 ** Copyright 2023 wahibre <wahibre@gmx.com>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#ifndef AUTOTOOLTIPDELEGATE_H
#define AUTOTOOLTIPDELEGATE_H

/*
 * from http://www.mimec.org/node/337
 */

#include <QObject>
#include <QStyledItemDelegate>
#include <QToolTip>
#include <QHelpEvent>
#include <QAbstractItemView>

class MyAutoToolTipDelegate : public QStyledItemDelegate
{
public:
    MyAutoToolTipDelegate(QObject *parent);
public slots:
    bool helpEvent(QHelpEvent* e, QAbstractItemView* view, const QStyleOptionViewItem& option,
                    const QModelIndex& index );

};

#endif // AUTOTOOLTIPDELEGATE_H
