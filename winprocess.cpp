/**
 ** This file is part of the Octopus project.
 ** Copyright 2023 wahibre <wahibre@gmx.com>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include <QMenu>
#include <QFile>
#include <QFileDialog>
#include <QMessageBox>
#include <QInputDialog>
#include <QSettings>
#include <QPropertyAnimation>
#include <QGraphicsEffect>
#include <QParallelAnimationGroup>
#include <QSequentialAnimationGroup>
#include <QSignalTransition>
#include <QFontDialog>
#include <QTextEdit>
#include <QCryptographicHash>
#include <QKeyEvent>
#include <QDesktopServices>
#include <QTabBar>
#include <QScrollBar>
#include <stdio.h>
#include <QDebug>

#include "winprocess.h"
#include "ui_winprocess.h"

/******************************************************************************************/
winProcess::winProcess(Prikaz prikaz, StartProcesu startProcesu, QWidget *parent):
    QDialog(parent),
    ui(new Ui::winProcess),
    startProcesu(startProcesu)
{
    ui->setupUi(this);

    cur_stdout_at_end = new QTextCursor(ui->textEdit->document());
    cur_stderr_at_end = new QTextCursor(ui->textBrowserErr->document());

    this->environmenty  = prikaz.dajEnvironmenty();
    this->program       = prikaz.dajProgram().simplified();
    this->parametre     = prikaz.dajParametre().filter(QRegExp(".+"));
    this->workdir       = prikaz.dajPracovnyPriecinok();
    this->processName   = QFileInfo(program).fileName();
    this->programHash   = QCryptographicHash::hash(program.toUtf8(), QCryptographicHash::Md5).toHex();
    this->zvyrazniVybraneIO =  new Zvyraznovac(ui->textEdit,       QColor(TAB_COLOR_SEL), this);
    this->zvyrazniVybraneErr=  new Zvyraznovac(ui->textBrowserErr, QColor(TAB_COLOR_SEL), this);
    ui->textBrowserErr->setTextColor(QColor(TAB_COLOR_ERR));

    setWindowTitle(processName);
    setWindowFlags(windowFlags() | Qt::WindowMaximizeButtonHint);
    setAttribute(Qt::WA_DeleteOnClose);
    setVisible(false);

    NastaveniaCitaj();
    if(!mojTextKodek)
        mojTextKodek = QTextCodec::codecForLocale();

    tmrDobaBehu = new QTimer(this);

    TrayOnIcon = QIcon(":/octopus");
    TrayOffIcon.addPixmap(TrayOnIcon.pixmap(64,64,QIcon::Disabled));

    initProcess();

    connect(tmrDobaBehu, SIGNAL(timeout()), this, SLOT(ObnovDobuBehu()));

    connect(&TrayIcon, SIGNAL(messageClicked()),                                this, SLOT(ZobrazOkno()) );
    connect(&TrayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)),    this, SLOT(ZobrazOkno()) );

    connect(ui->actionSaveStdOutLog, SIGNAL(triggered()), this, SLOT(SaveStdOutlogfile()));
    connect(ui->actionSaveStdErrLog, SIGNAL(triggered()), this, SLOT(SaveStdErrlogfile()));

    connect(ui->textEdit, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(MyContextMenuStdOut(QPoint)));
    connect(ui->textBrowserErr, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(MyContextMenuStdErr(QPoint)));
    connect(ui->btnWrite, SIGNAL(clicked()), this, SLOT(Zapisovanie()));

    nastavAutomat();
}
/******************************************************************************************/
void winProcess::initProcess()
{
    ui->label->setText(tr("Čakám na štart ..."));
    ui->lcdNumber->display("00:00");
    ui->groupBox->setChecked(false);
    ui->edtWrite->clear();

    casSpustenia = casUkoncenia = QDateTime::currentDateTime();

    proc = new QProcess();
    proc->setProcessChannelMode(QProcess::SeparateChannels);

    if(!environmenty.isEmpty())
    {
        QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
        foreach(auto i, environmenty.keys())
        {
            if(environmenty[i].contains(ENVIRON_CHAR))
                env.insert(i, GetStdoutFromCommand(QString("echo %1").arg(environmenty[i])));
            else
                env.insert(i, environmenty[i]);
        }
        proc->setProcessEnvironment(env);
    }

    QDir D = QDir(workdir);
    if(D.exists(workdir))
        proc->setWorkingDirectory(workdir);
    else
        qWarning() << tr("Pracovný priečinok %1 neexistuje").arg(workdir);

    connect(proc, SIGNAL(started()),                 this, SLOT(Started()) );
    connect(proc, SIGNAL(readyReadStandardOutput()), this, SLOT(Citanie()) );
    connect(proc, SIGNAL(readyReadStandardError()),  this, SLOT(CitanieErr()) );
    connect(proc, SIGNAL(finished(int)),             this, SLOT(Ukoncenie(int)) );
}
/******************************************************************************************/
void winProcess::Restart()
{
    killProcess();

    for(int i = 0; i<2; i++)
        ui->tabWidget->tabBar()->setTabTextColor(i, QApplication::palette().text().color());

    ui->textEdit->clear();
    ui->textBrowserErr->clear();

    cur_stdout_at_end->movePosition(QTextCursor::End);
    cur_stderr_at_end->movePosition(QTextCursor::End);

    tmrDobaBehu->stop();

    delete automat;
    delete proc;

    TrayIcon.hide();

    initProcess();
    nastavAutomat();
    runProcess();
}
/******************************************************************************************/
void winProcess::killProcess()
{
    if (proc->state() != QProcess::NotRunning)
    {
        proc->kill();
        proc->waitForFinished();
    }
}
/******************************************************************************************/
winProcess::~winProcess()
{
    NastaveniaZapis();

    killProcess();
    delete proc;
    delete ui;
}
/******************************************************************************************/
bool winProcess::jeProcesAktivny()
{
    return (proc->state() != QProcess::NotRunning);
}
/******************************************************************************************/
QString winProcess::nazovProgramu()
{
    return QFileInfo(program).baseName();
}
/******************************************************************************************/
void winProcess::reject()
{
    if( jeProcesAktivny() &&
        (QMessageBox::question(this, tr("Otázka"), tr("Proces stále beží, naozaj ho chcete ukončiť?"), QMessageBox::Yes|QMessageBox::No, QMessageBox::No)
                            == QMessageBox::No)
    )
        return;

    QDialog::reject();
}
/******************************************************************************************/
void winProcess::keyReleaseEvent(QKeyEvent * ev)
{
/*
 * Alt + 1   zalozka StdOut
 * Alt + 2   zalozka StdErr                                                         */

    if((ev->key() == Qt::Key_1 || ev->key() == Qt::Key_2) && ev->modifiers() == Qt::AltModifier)
    {
        if(ev->key() == Qt::Key_1)
            ui->tabWidget->setCurrentIndex(TAB_STD_OUT);
        else
            ui->tabWidget->setCurrentIndex(TAB_STD_ERR);

        ev->setAccepted(true);
        return;
    }

/*
 *       F3     find
 * Shift+F3     find backwards                                                          */

    QTextEdit *editor;

    const bool spracujemUdalost = (
                ev->key() == Qt::Key_F3 &&
                (ev->modifiers() == Qt::ShiftModifier || ev->modifiers() == Qt::NoModifier)
    );

    ev->setAccepted(spracujemUdalost);

    if(spracujemUdalost)
    {
        const bool spatneHladanie = ev->modifiers() == Qt::ShiftModifier;

        switch(ui->tabWidget->currentIndex())
        {
        case TAB_STD_OUT:
            editor = ui->textEdit;
            break;
        case TAB_STD_ERR:
            editor = ui->textBrowserErr;
            break;
        default:
            QMessageBox::critical(this, tr("Chyba"), tr("Nesprávny index záložky!"));
            return;
        }

        if(spatneHladanie)
            editor->find(editor->textCursor().selectedText(), QTextDocument::FindBackward);
        else
            editor->find(editor->textCursor().selectedText());
    }
    else
        QDialog::keyReleaseEvent(ev);
}
/******************************************************************************************/
void winProcess::NastaveniaZapis()
{    
     QScopedPointer<QSettings> nastavenia(new QSettings(this));

    nastavenia->beginGroup(programHash);
    if(!isMaximized())
        nastavenia->setValue("geometry", geometry());
    nastavenia->setValue("encoding", mojTextKodek->name());
    nastavenia->setValue("font", ui->textEdit->font().toString());
    nastavenia->endGroup();
}
/******************************************************************************************/
void winProcess::NastaveniaCitaj()
{
    QScopedPointer<QSettings>   nastavenia(new QSettings(this));
    QFont       nastavenyFont;

    nastavenia->beginGroup(programHash);

    QByteArray  nazovKodeku = nastavenia->value("encoding", "?").toString().toLatin1();
    nastavenyFont.fromString(nastavenia->value("font", "").toString());

    if(nastavenia->contains("geometry"))
        this->setGeometry(nastavenia->value("geometry", QRect()).toRect());

    mojTextKodek = QTextCodec::codecForName(nazovKodeku);

    if(!nastavenyFont.family().isEmpty())
    {
        ui->textEdit->setFont(nastavenyFont);
        ui->textBrowserErr->setFont(nastavenyFont);
    }
    nastavenia->endGroup();
}
/******************************************************************************************/
bool winProcess::ZmenKodovanie(QString novyNazovKodeku)
{
    QString vybranyNazovKodeku;
    QTextCodec *novyTextCodek;

    /* vybery z dostupnych kodekov */
    if(novyNazovKodeku.isEmpty())
    {
        bool ok;

        nacitajDostupneKodeky();

        vybranyNazovKodeku = QInputDialog::getItem(this, tr("Výber"), tr("Zvoľte znakovú sadu:"), dostupneKodeky, dostupneKodeky.indexOf(mojTextKodek->name()), false, &ok);

        if(ok && !vybranyNazovKodeku.isEmpty())
            novyNazovKodeku = vybranyNazovKodeku;
        else
            return false;
    }

    novyTextCodek = QTextCodec::codecForName(novyNazovKodeku.toLatin1());

    if(!novyTextCodek)
    {
        QMessageBox::warning(this, tr("Warovanie"), tr("Nie je možné nastaviť kodek: %1").arg(novyNazovKodeku));
        return false;
    }

    mojTextKodek = novyTextCodek;

    ui->textEdit->setPlainText(mojTextKodek->toUnicode(outputStdOutText));
    ui->textBrowserErr->setPlainText(mojTextKodek->toUnicode(outputStdErrText));

    return true;
}
/******************************************************************************************/
void winProcess::ZmenFont()
{
    bool ok;
    QFont novyFont;

    novyFont = QFontDialog::getFont(&ok, ui->textEdit->font(), this);

    if (ok)
    {
        ui->textEdit->setFont(novyFont);
        ui->textBrowserErr->setFont(novyFont);
    }
}
/******************************************************************************************/
void winProcess::Citanie()
{
    QMutexLocker ml(&readingProcess);
    QByteArray textToInsert;

    QScrollBar *p_scroll_bar = ui->textEdit->verticalScrollBar();
    bool bool_at_bottom = (p_scroll_bar->value() == p_scroll_bar->maximum());

    textToInsert = proc->readAllStandardOutput();
    outputStdOutText.append(textToInsert);
    cur_stdout_at_end->insertText(mojTextKodek->toUnicode(textToInsert));

    if (bool_at_bottom)
        p_scroll_bar->setValue(p_scroll_bar->maximum());

    QTabBar *f = ui->tabWidget->tabBar();
    if (f->currentIndex() != TAB_STD_OUT)
        f->setTabTextColor(TAB_STD_OUT, QColor(TAB_COLOR_OUT));
}
/******************************************************************************************/
void winProcess::CitanieErr()
{
    QMutexLocker ml(&readingProcess);
    QByteArray textToInsert;

    QScrollBar *p_scroll_bar = ui->textBrowserErr->verticalScrollBar();
    bool bool_at_bottom = (p_scroll_bar->value() == p_scroll_bar->maximum());

    textToInsert = proc->readAllStandardError();
    outputStdErrText.append(textToInsert);
    cur_stderr_at_end->insertText(mojTextKodek->toUnicode(textToInsert));

    if (bool_at_bottom)
        p_scroll_bar->setValue(p_scroll_bar->maximum());

    QTabBar *f = ui->tabWidget->tabBar();
    if (f->currentIndex() != TAB_STD_ERR)
        f->setTabTextColor(TAB_STD_ERR, TAB_COLOR_ERR);
}
/******************************************************************************************/
void winProcess::Zapisovanie()
{
    proc->write(ui->edtWrite->text().toLatin1());
    ui->btnWrite->setEnabled(false);
    proc->waitForBytesWritten();
    proc->closeWriteChannel();

    ui->btnWrite->setEnabled(true);
    ui->edtWrite->clear();
}
/******************************************************************************************/
void winProcess::ObnovDobuBehu()
{
    static int sekunda=0;
    QTime rozdiel(0, 0, 0, 0);
    QString znak, cas;

    duration = dobaBehu;

    rozdiel = rozdiel.addMSecs(duration.elapsed());
    if(sekunda == rozdiel.second())
        znak = " ";
    else
    {
        znak = ":";
        sekunda = rozdiel.second();
    }

    cas = rozdiel.toString("mm"+znak+"ss");
    ui->lcdNumber->display(cas);

    cas = rozdiel.toString("mm:ss");
    TrayIcon.setToolTip(processName + QString(": %1 (%2s)").arg(statusMsg).arg(cas));
}
/******************************************************************************************/
void winProcess::Started()
{
    statusMsg = tr("Beží")+"...";
    dobaBehu.start();
    tmrDobaBehu->start(500);
    ui->label->setText(statusMsg);

    TrayIcon.setIcon(TrayOffIcon);
    TrayIcon.setToolTip(processName + ": " + statusMsg);
    TrayIcon.show();

    emit processStarted();
}
/******************************************************************************************/
void winProcess::Ukoncenie(int vysledok)
{
    int msecs;

    returnCode = vysledok;
    casUkoncenia = QDateTime::currentDateTime();
    statusMsg = QString("Finished (%1) at %2").arg(vysledok).arg(casUkoncenia.toString("hh:mm:ss"));

    if(isActiveWindow())
        msecs = 7000;       //7s.
    else
        msecs = 1800000;    //30min.

    ui->label->setText(statusMsg);
    tmrDobaBehu->stop();

    emit processFinished();

    if(startProcesu==beziaci_vzadu && vysledok==_O_K_)
    {
        TrayIcon.hide();
        accept();
    }
    else
    {
        TrayIcon.setIcon(TrayOnIcon);
        TrayIcon.setVisible(true);
        TrayIcon.setToolTip(processName + ": " + statusMsg);
        TrayIcon.showMessage(windowTitle(), statusMsg, QSystemTrayIcon::Information, msecs);
    }
}
/******************************************************************************************/
void winProcess::SaveStdOutlogfile()
{
    SaveLogFile(outputStdOutText, "log");
}
/******************************************************************************************/
void winProcess::SaveStdErrlogfile()
{
    SaveLogFile(outputStdErrText, "err");
}
/******************************************************************************************/
void winProcess::SaveLogFile(QByteArray outText, QString fExtension)
{
    QString Subor, FileSuffix = fExtension;

    Subor = dateTimeFileName();

    Subor = QFileDialog::getSaveFileName(this, tr("Uložiť log súbor"), Subor, QString("*.%1").arg(FileSuffix));
    if(Subor.isEmpty())
        return;

    QFileInfo fi(Subor);

    if(fi.suffix().isEmpty())
        Subor += "."+FileSuffix;

    QScopedPointer<QFile> f_subor(new QFile(Subor, this));

    if(f_subor->open(QIODevice::WriteOnly))
    {
        if(f_subor->write(outText) == -1)
            QMessageBox::critical(this, tr("Chyba"), tr("Nepodarilo sa zapisat vystup programu do logu \"%1\"").arg(Subor));
        f_subor->close();
    }
    else
        qWarning()<<tr("Nie je mozne otvorit subor")<<Subor;
//    delete f_subor;
}
/******************************************************************************************/
void winProcess::ZobrazOkno()
{
    if(!isVisible())
        show();
    else if(!isActiveWindow())
        activateWindow();
}
/******************************************************************************************/
void winProcess::ExportHtml()
{
    QFile fileIn(":/rsc/out_template.html");

    if(fileIn.open(QIODevice::ReadOnly))
    {
        QString Subor = dateTimeFileName() + ".html";

        Subor = QFileDialog::getSaveFileName(this, tr("Uložiť HTML súbor"), Subor, "*.html");

        if(Subor.isEmpty())
            return;

        QTextStream streamIn(&fileIn);
        QString htmltext = streamIn.readAll();
        QString htmlTable;
        QFile fileOut(Subor);
        QTextStream streamOut(&fileOut);
        QTime dobaBehu(0, 0, 0, 0);

        // Page's Metadata
        QString htmlMetadata = QString(
        "<meta name=\"generator\" content=\"%1\"/>\n"
        "<meta name=\"created\" content=\"%2\"/>"
        ).arg(QString("%1 %2 (%3)").arg(qAppName()).arg(VERZIA).arg(BUILDDATE)).arg(QDateTime::currentDateTime().toString("yyyy-MM-dd"));

        // Program info
        htmlTable += htmlTableRow(tr("Program"),    program);
        htmlTable += htmlTableRow(tr("Parametre"),  parametre.join(' '));
        htmlTable += htmlTableRow(tr("Start"),      casSpustenia.toString("dd.MM.yyyy HH:mm:ss"));
        htmlTable += htmlTableRow(tr("Stop"),       jeProcesAktivny() ? "..." : casUkoncenia.toString("dd.MM.yyyy HH:mm:ss"));
        htmlTable += htmlTableRow(tr("Trvanie"),    duration.elapsed()>0? dobaBehu.addMSecs(duration.elapsed()).toString("m:ss") : "0:00");
        htmlTable += htmlTableRow(tr("Status"),     jeProcesAktivny() ? "..." : QString::number(returnCode));

        // Used font
        QString htmlFont = encodeCssFont(ui->textEdit->font()) + ";";

        // insert into html template
        htmltext.replace("$$placeholcer_for_metadata$$", htmlMetadata);
        htmltext.replace("$$placeholcer_for_content_font$$", htmlFont);
        htmltext.replace("$$placeholcer_for_table$$", htmlTable);
        htmltext.replace("$$placeholcer_for_standard_output$$", QString(outputStdOutText).toHtmlEscaped());
        htmltext.replace("$$placeholcer_for_standard_error$$", QString(outputStdErrText).toHtmlEscaped());

        // write final html page to disk
        if(fileOut.open(QIODevice::WriteOnly))
        {
            streamOut << htmltext;
            fileOut.close();
        }
        else
            QMessageBox::warning(this, tr("Chyba"), tr(u8"Nie je možné otvoriť súbor \"%1\" na zápis!").arg(fileIn.fileName()));

        fileIn.close();

        if(QMessageBox::question(this, tr("HTML report"), tr(u8"Prajete si otvoriť report?"), QMessageBox::Yes|QMessageBox::Default, QMessageBox::No)
                == QMessageBox::Yes)
            QDesktopServices::openUrl(Subor);
    }
    else
        qWarning() << tr(u8"Nie je možné čítať zdroj: %1").arg(fileIn.fileName());
}
/******************************************************************************************/
void winProcess::MyContextMenu(MyTextBrowser *browser, QAction *saveAction, QPoint bod)
{
//    contextMenu->setStyleSheet("* { menu-scrollable: 1 }");

    QAction ActEncSel(tr("&Znaková sada"), this);
    QAction ActFontSel(tr("&Font"), this);
    QAction ActExpHtml(tr("&Export do HTML"), this);
    QAction ActRestart(tr("&Reštart procesu"), this);
    QScopedPointer<QMenu> contextMenu(browser->createStandardContextMenu(bod));

    contextMenu->addSeparator();
    contextMenu->addAction(&ActEncSel);
    contextMenu->addAction(&ActFontSel);
    contextMenu->addSeparator();
    contextMenu->addAction(&ActExpHtml);
    contextMenu->addAction(saveAction);
    contextMenu->addSeparator();
    contextMenu->addAction(&ActRestart);

    connect(&ActEncSel,  SIGNAL(triggered()), this, SLOT(ZmenKodovanie()));
    connect(&ActFontSel, SIGNAL(triggered()), this, SLOT(ZmenFont()));
    connect(&ActExpHtml, SIGNAL(triggered()), this, SLOT(ExportHtml()));
    connect(&ActRestart, SIGNAL(triggered()), this, SLOT(Restart()));

    contextMenu->exec(browser->mapToGlobal(bod));
}
/******************************************************************************************/
void winProcess::nastavAutomat()
{
    automat = new QStateMachine(/*QState::ParallelStates,*/ this);
    QState *parState = new QState(QState::ParallelStates, automat);

    QState *viditelnost = new QState(QState::ExclusiveStates,parState);
    QState *moze = new QState(QState::ExclusiveStates,parState);

    QState *vidiAno = new QState(viditelnost);
    QState *vidiNie = new QState(viditelnost);
    QState *mozeAno = new QState(moze);
    QState *mozeNie = new QState(moze);

    vidiAno->assignProperty(ui->groupBox, "maximumHeight", 60);
    vidiNie->assignProperty(ui->groupBox, "maximumHeight", 20);
    mozeNie->assignProperty(ui->groupBox, "enabled", false);
    mozeAno->assignProperty(ui->groupBox, "enabled", true);

    QSignalTransition *tNieAno = vidiNie->addTransition(ui->groupBox, SIGNAL(toggled(bool)), vidiAno);
    QSignalTransition *tAnoNie = vidiAno->addTransition(ui->groupBox, SIGNAL(toggled(bool)), vidiNie);
    mozeNie->addTransition(proc, SIGNAL(started()), mozeAno);
    mozeAno->addTransition(proc, SIGNAL(finished(int)), mozeNie);

    QPropertyAnimation *animaciaMaxHout = new QPropertyAnimation(ui->groupBox, "maximumHeight");
    animaciaMaxHout->setEasingCurve(QEasingCurve::OutBack);
    animaciaMaxHout->setDuration(DOBA_ANIMACIE);
    QPropertyAnimation *animaciaMaxHin = new QPropertyAnimation(ui->groupBox, "maximumHeight");
    animaciaMaxHin->setEasingCurve(QEasingCurve::InBack);
    animaciaMaxHin->setDuration(DOBA_ANIMACIE);

    tNieAno->addAnimation(animaciaMaxHin);
    tAnoNie->addAnimation(animaciaMaxHout);

    automat->setInitialState(parState);
    viditelnost->setInitialState(vidiNie);
    moze->setInitialState(mozeAno);
    automat->start();
}
/******************************************************************************************/
void winProcess::runProcess()
{
#if (QT_VERSION >= QT_VERSION_CHECK(5, 10, 0))
    if(startProcesu == beziaci_mimo)
    {
        proc->setProgram(program);
        proc->setArguments(parametre);
        proc->setWorkingDirectory(workdir);
        //proc->setProcessEnvironment();        //nastavene v konstruktore

        if(!proc->startDetached())
        {
            qCritical() << proc->errorString();
            QMessageBox::warning(this, tr("Warovanie"), QString("%1\n%2").arg(tr("Nie je možné spustiť proces!").arg(proc->errorString())));
        }
        accept();
    }
    else
#else
    startProcesu = beziaci_vpredu;
#endif
    {

        proc->start(program, parametre, QIODevice::ReadWrite|QIODevice::Unbuffered);
        //info label
        if (!proc->waitForStarted())
        {
            qCritical() << proc->errorString();
            ui->label->setText("Failed!");
        }

        if(startProcesu == beziaci_vpredu)
            show();
    }
}
/******************************************************************************************/
QString winProcess::htmlTableRow(QString name, QString value)
{
    return QString(  "<tr>\n"
                     "  <td bgcolor=\"#5E8AC7\"><font color=\"#FFFFFF\">%1</font></td>\n"
                     "  <td bgcolor=white>%2</td>\n"
                     "</tr>\n"
                     ).arg(name.toHtmlEscaped()).arg(value.toHtmlEscaped());

}
/******************************************************************************************/
QString winProcess::dateTimeFileName()
{
    return this->program + "_" + casUkoncenia.toString("yyyyMMdd_hh_mm_ss").replace(".", "_" );
}
/******************************************************************************************/
void winProcess::MyContextMenuStdOut(QPoint bod)
{
    MyContextMenu(ui->textEdit, ui->actionSaveStdOutLog, bod);
}
/******************************************************************************************/
void winProcess::MyContextMenuStdErr(QPoint bod)
{
    MyContextMenu(ui->textBrowserErr, ui->actionSaveStdErrLog, bod);
}
/******************************************************************************************/
void winProcess::nacitajDostupneKodeky()
{
    if (dostupneKodeky.isEmpty())
    {
        QList<QByteArray> kodekyAvail = QTextCodec::availableCodecs();

        foreach(QByteArray i, kodekyAvail)
            dostupneKodeky.append(QString(i));

        dostupneKodeky.removeDuplicates();
        dostupneKodeky.sort(Qt::CaseInsensitive);
    }
}
/******************************************************************************************/
void winProcess::on_tabWidget_currentChanged(int index)
{
    ui->tabWidget->tabBar()->setTabTextColor(index, QApplication::palette().text().color());
}
/******************************************************************************************/
void winProcess::on_textEdit_selectionChanged()
{
    if(QApplication::keyboardModifiers() & Qt::ControlModifier)
        zvyrazniVybraneIO->zvyrazniVybrane();
}
/******************************************************************************************/
void winProcess::on_textBrowserErr_selectionChanged()
{
    if(QApplication::keyboardModifiers() & Qt::ControlModifier)
        zvyrazniVybraneErr->zvyrazniVybrane();
}
/******************************************************************************************/
QString winProcess::encodeCssFont (const QFont& refFont)
{
    //-----------------------------------------------------------------------
    // This function assembles a CSS Font specification string from
    // a QFont. This supports most of the QFont attributes settable in
    // the Qt 4.8 and Qt 5.3 QFontDialog.
    //
    // (1) Font Family
    // (2) Font Weight (just bold or not)
    // (3) Font Style (possibly Italic or Oblique)
    // (4) Font Size (in either pixels or points)
    // (5) Decorations (possibly Underline or Strikeout)
    //
    // Not supported: Writing System (e.g. Latin).
    //
    // See the corresponding decode function, below.
    // QFont decodeCssFontString (const QString cssFontStr)
    //-----------------------------------------------------------------------

    QStringList fields; // CSS font attribute fields

    // ***************************************************
    // *** (1) Font Family: Primary plus Substitutes ***
    // ***************************************************

    const QString family = refFont.family();

    // Note: [9-2014, Qt 4.8.6]: This isn't what I thought it was. It
    // does not return a list of "fallback" font faces (e.g. Georgia,
    // Serif for "Times New Roman"). In my testing, this is always
    // returning an empty list.
    //
    QStringList famSubs = QFont::substitutes (family);

    if (!famSubs.contains (family))
    famSubs.prepend (family);

    const QChar DBL_QUOT ('"');
    const int famCnt = famSubs.count();
    QStringList famList;
    for (int inx = 0; inx < famCnt; ++inx)
    {
    // Place double quotes around family names having space characters,
    // but only if double quotes are not already there.
    //
    const QString fam = famSubs [inx];
    if (fam.contains (' ') && !fam.startsWith (DBL_QUOT))
    famList << (DBL_QUOT + fam + DBL_QUOT);
    else
    famList << fam;
    }

    const QString famStr = QString ("font-family: ") + famList.join (", ");
    fields << famStr;

    // **************************************
    // *** (2) Font Weight: Bold or Not ***
    // **************************************

    const bool bold = refFont.bold();
    if (bold)
        fields << "font-weight: bold";

    // ****************************************************
    // *** (3) Font Style: possibly Italic or Oblique ***
    // ****************************************************

    const QFont::Style style = refFont.style();
    switch (style)
    {
        case QFont::StyleNormal: break;
        case QFont::StyleItalic: fields << "font-style: italic"; break;
        case QFont::StyleOblique: fields << "font-style: oblique"; break;
    }

    // ************************************************
    // *** (4) Font Size: either Pixels or Points ***
    // ************************************************

    const double sizeInPoints = refFont.pointSizeF(); // <= 0 if not defined.
    const int sizeInPixels = refFont.pixelSize(); // <= 0 if not defined.
    if (sizeInPoints > 0.0)
        fields << QString ("font-size: %1pt") .arg (sizeInPoints);
    else if (sizeInPixels > 0)
        fields << QString ("font-size: %1px") .arg (sizeInPixels);

    // ***********************************************
    // *** (5) Decorations: Underline, Strikeout ***
    // ***********************************************

    const bool underline = refFont.underline();
    const bool strikeOut = refFont.strikeOut();

    if (underline && strikeOut)
        fields << "text-decoration: underline line-through";
    else if (underline)
        fields << "text-decoration: underline";
    else if (strikeOut)
        fields << "text-decoration: line-through";

    const QString cssFontStr = fields.join ("; ");
    return cssFontStr;
}
/******************************************************************************************/
QString winProcess::GetStdoutFromCommand(QString cmd) {
// Source: https://www.jeremymorgan.com/tutorials/c-programming/how-to-capture-the-output-of-a-linux-command-in-c/
    QString data;
    FILE* stream;
    const int max_buffer = 256;
    char buffer[max_buffer];
    //cmd.append(" 2>&1");

    stream = popen(cmd.toStdString().c_str(), "r");
    if (stream)
    {
        while (!feof(stream))
            if (fgets(buffer, max_buffer, stream) != NULL)
                data.append(buffer);
        pclose(stream);
    }
    return data.trimmed();
}
/******************************************************************************************/
