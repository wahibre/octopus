/**
 ** This file is part of the Octopus project.
 ** Copyright 2023 wahibre <wahibre@gmx.com>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#ifndef MYLINEEDIT_H
#define MYLINEEDIT_H

#include <QDropEvent>
#include <QLineEdit>

#define O_MIME_LINEEDIT "Octopus/LineEdit"

class MyLineEdit : public QLineEdit
{
    Q_OBJECT
public:
    MyLineEdit(QWidget *parent);
    void setIndexUmiestnenia(int idx);
private:
    int indexUmiestnenia;
    bool isSelectionNumber(int &outNumber);
    bool isSelectionDate(QDate &outDate);
    bool isLastDayOfMonth(QDate datum);
    bool increaseSelectedText(int step, Qt::KeyboardModifiers mod);
    QString increaseDateText(QDate datum, int step, Qt::KeyboardModifiers mod);

    virtual void dropEvent(QDropEvent *e)               Q_DECL_OVERRIDE;
    virtual void dragEnterEvent(QDragEnterEvent * e)    Q_DECL_OVERRIDE;
    virtual void mousePressEvent(QMouseEvent *e)        Q_DECL_OVERRIDE;
    virtual void keyPressEvent(QKeyEvent *event)        Q_DECL_OVERRIDE;
    virtual void wheelEvent(QWheelEvent *event)         Q_DECL_OVERRIDE;
    virtual void contextMenuEvent(QContextMenuEvent *event) Q_DECL_OVERRIDE;
signals:
    void ziadostNaVymenu(int indexZdroja, int indexCiela);
    void ziadostNaFilter(QString vzor);
};

#endif // MYLINEEDIT_H
