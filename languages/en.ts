<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>FindTextWidget</name>
    <message>
        <location filename="../findtextwidget.h" line="15"/>
        <source>Zadaj text na vyhľadávanie</source>
        <translation>Enter Text to find</translation>
    </message>
    <message>
        <location filename="../findtextwidget.h" line="16"/>
        <source>Rozlišovať veľké písmená</source>
        <translation>Case sensitive</translation>
    </message>
    <message>
        <location filename="../findtextwidget.h" line="17"/>
        <source>Späť</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="../findtextwidget.h" line="18"/>
        <source>Ďalej</source>
        <translation>Next</translation>
    </message>
</context>
<context>
    <name>HistoriaModel</name>
    <message>
        <location filename="../historiamodel.cpp" line="18"/>
        <source>Program</source>
        <translation>Program</translation>
    </message>
    <message>
        <location filename="../historiamodel.cpp" line="19"/>
        <source>Parametre</source>
        <translation>Parameters</translation>
    </message>
    <message>
        <location filename="../historiamodel.cpp" line="61"/>
        <source>Nie je mozne vytvorit priecinok</source>
        <translation>Cannot create directory</translation>
    </message>
    <message>
        <location filename="../historiamodel.cpp" line="350"/>
        <source>Mazanie viacero riadkov v modeli súčasne nie je podporované!</source>
        <translation>Multiple rows removal in datamodel is not allowed!</translation>
    </message>
</context>
<context>
    <name>MyLineEdit</name>
    <message>
        <location filename="../mylineedit.cpp" line="13"/>
        <source>Vyznačenú časť textu (celé číslo alebo dátum) je možné inkrementovať/dekrementovať pomocou NUM+, NUM-, NUM* alebo kolieskom myši</source>
        <translatorcomment>Vyznačenú časť textu (celé číslo alebo dátum) je možné inkrementovať/dekrementovať pomocou NUM+, NUM-, NUM* alebo kolieskom myši</translatorcomment>
        <translation>To increment/decrement selected piece of text (number or date) you can use numeric keys NUM+, NUM-,NUM* or mouse scroll wheel</translation>
    </message>
    <message>
        <location filename="../mylineedit.cpp" line="234"/>
        <source>&amp;Filter v histórii na</source>
        <translation>&amp;Filter history to</translation>
    </message>
    <message>
        <location filename="../mylineedit.cpp" line="235"/>
        <source>&amp;Vložiť hodnotu</source>
        <translation>&amp;Insert value</translation>
    </message>
</context>
<context>
    <name>MyProgramLineEdit</name>
    <message>
        <location filename="../myprogramlineedit.cpp" line="17"/>
        <source>&amp;Filter v histórii na</source>
        <translation>&amp;Filter history to</translation>
    </message>
</context>
<context>
    <name>MyTextBrowser</name>
    <message>
        <location filename="../mytextbrowser.h" line="18"/>
        <source>Hľadaj...</source>
        <translation>Find...</translation>
    </message>
    <message>
        <location filename="../mytextbrowser.h" line="19"/>
        <source>Zalamovanie</source>
        <translation>Word wrap</translation>
    </message>
</context>
<context>
    <name>VersionDownloader</name>
    <message>
        <location filename="../versiondownloader.cpp" line="93"/>
        <source>changelog XML je v nesprávnom formáte</source>
        <translation>wrong XML format in changelog</translation>
    </message>
    <message>
        <location filename="../versiondownloader.cpp" line="105"/>
        <source>Chyba spojenia:</source>
        <translation>Connection error:</translation>
    </message>
    <message>
        <location filename="../versiondownloader.cpp" line="111"/>
        <source>Chyba SSL spojenia:</source>
        <translation>SSL Connection error:</translation>
    </message>
</context>
<context>
    <name>winEnvironment</name>
    <message>
        <location filename="../winenvironment.ui" line="17"/>
        <source>Environmentálne premenné</source>
        <translation>Environment variables</translation>
    </message>
    <message>
        <location filename="../winenvironment.ui" line="45"/>
        <source>Premenná</source>
        <translation>Variable</translation>
    </message>
    <message>
        <location filename="../winenvironment.ui" line="50"/>
        <source>Hodnota</source>
        <translation>Value</translation>
    </message>
    <message>
        <location filename="../winenvironment.ui" line="62"/>
        <source>Zmazať &amp;všetko</source>
        <translation>Delete &amp;All</translation>
    </message>
    <message>
        <location filename="../winenvironment.ui" line="65"/>
        <source>Zmazať všetko</source>
        <translation>Delete All</translation>
    </message>
    <message>
        <location filename="../winenvironment.ui" line="80"/>
        <source>&amp;Zmazať</source>
        <translation>&amp;Delete</translation>
    </message>
    <message>
        <location filename="../winenvironment.ui" line="83"/>
        <source>Zmazať</source>
        <translation>Delete</translation>
    </message>
    <message>
        <location filename="../winenvironment.ui" line="98"/>
        <source>&amp;Kopírovať</source>
        <translation>&amp;Copy</translation>
    </message>
    <message>
        <location filename="../winenvironment.ui" line="101"/>
        <source>Kopírovať</source>
        <translation>Copy</translation>
    </message>
    <message>
        <location filename="../winenvironment.ui" line="116"/>
        <source>&amp;Vložiť</source>
        <translation>&amp;Paste</translation>
    </message>
    <message>
        <location filename="../winenvironment.ui" line="119"/>
        <source>Vložiť</source>
        <translation>Paste</translation>
    </message>
    <message>
        <location filename="../winenvironment.ui" line="104"/>
        <source>Ctrl+C</source>
        <translation>Ctrl+C</translation>
    </message>
    <message>
        <location filename="../winenvironment.ui" line="122"/>
        <source>Ctrl+V</source>
        <translation>Ctrl+V</translation>
    </message>
    <message>
        <location filename="../winenvironment.ui" line="68"/>
        <source>Ctrl+Del</source>
        <translation>Ctrl+Del</translation>
    </message>
    <message>
        <location filename="../winenvironment.ui" line="86"/>
        <source>Del</source>
        <translation>Del</translation>
    </message>
</context>
<context>
    <name>winMain</name>
    <message>
        <location filename="../winmain.ui" line="14"/>
        <source>Octopus</source>
        <translation>Octopus</translation>
    </message>
    <message>
        <location filename="../winmain.ui" line="44"/>
        <source>P&amp;rogram</source>
        <translation>P&amp;rogram</translation>
    </message>
    <message>
        <location filename="../winmain.ui" line="89"/>
        <location filename="../winmain.cpp" line="645"/>
        <source>Načítať binárku</source>
        <translation>Load binary</translation>
    </message>
    <message>
        <location filename="../winmain.ui" line="114"/>
        <source>P&amp;arametre</source>
        <translation>P&amp;arameters</translation>
    </message>
    <message>
        <location filename="../winmain.ui" line="146"/>
        <source>Pridať parametre</source>
        <translation>Add parameters</translation>
    </message>
    <message>
        <location filename="../winmain.ui" line="178"/>
        <source>Odstrániť parametre</source>
        <translation>Remove parameters</translation>
    </message>
    <message>
        <location filename="../winmain.ui" line="204"/>
        <location filename="../winmain.ui" line="613"/>
        <source>Environmentálne premenné</source>
        <translation>Environment variables</translation>
    </message>
    <message>
        <location filename="../winmain.cpp" line="33"/>
        <location filename="../winmain.cpp" line="1000"/>
        <source>Spustiť proces</source>
        <translation>Run process</translation>
    </message>
    <message>
        <location filename="../winmain.ui" line="307"/>
        <source>&amp;História</source>
        <translation>&amp;History</translation>
    </message>
    <message>
        <location filename="../winmain.ui" line="374"/>
        <source>Po&amp;moc</source>
        <translation>&amp;Help</translation>
    </message>
    <message>
        <location filename="../winmain.ui" line="381"/>
        <source>S&amp;úbor</source>
        <translation>&amp;File</translation>
    </message>
    <message>
        <location filename="../winmain.ui" line="392"/>
        <source>&amp;Upraviť</source>
        <translation>&amp;Edit</translation>
    </message>
    <message>
        <location filename="../winmain.ui" line="405"/>
        <source>&amp;Zobraziť</source>
        <translation>&amp;View</translation>
    </message>
    <message>
        <location filename="../winmain.ui" line="423"/>
        <source>O&amp;dstrániť z histórie</source>
        <translation>&amp;Remove from history</translation>
    </message>
    <message>
        <location filename="../winmain.ui" line="426"/>
        <source>Odstrániť vybrané položky z histórie</source>
        <translation>Remove selected Items from History</translation>
    </message>
    <message>
        <location filename="../winmain.ui" line="429"/>
        <source>Shift+Del</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../winmain.ui" line="439"/>
        <source>About &amp;Qt</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../winmain.ui" line="442"/>
        <source>About Qt</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../winmain.ui" line="454"/>
        <location filename="../winmain.ui" line="457"/>
        <source>&amp;Spustiť proces</source>
        <translation>&amp;Run process</translation>
    </message>
    <message>
        <location filename="../winmain.ui" line="460"/>
        <source>F5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../winmain.ui" line="469"/>
        <location filename="../winmain.ui" line="472"/>
        <source>&amp;Ukončiť</source>
        <translation>&amp;Quit</translation>
    </message>
    <message>
        <location filename="../winmain.ui" line="475"/>
        <source>Ctrl+Q</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../winmain.ui" line="484"/>
        <source>&amp;Pridať parametre</source>
        <translation>&amp;Add parameters</translation>
    </message>
    <message>
        <location filename="../winmain.ui" line="487"/>
        <source>Ins</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../winmain.ui" line="496"/>
        <source>&amp;Odstrániť parametre</source>
        <translation>R&amp;emove parameters</translation>
    </message>
    <message>
        <location filename="../winmain.ui" line="499"/>
        <source>Del</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../winmain.ui" line="508"/>
        <source>&amp;About</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../winmain.ui" line="511"/>
        <source>F1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../winmain.ui" line="520"/>
        <source>&amp;Vložiť parametre</source>
        <translation>&amp;Insert parameters</translation>
    </message>
    <message>
        <location filename="../winmain.ui" line="529"/>
        <source>&amp;Zmeniť prac. priečinok</source>
        <translation>Change &amp;working Directory</translation>
    </message>
    <message>
        <location filename="../winmain.ui" line="532"/>
        <source>Zmeniť prac. priečinok</source>
        <translation>Change working Directory</translation>
    </message>
    <message>
        <location filename="../winmain.ui" line="535"/>
        <source>F2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../winmain.ui" line="547"/>
        <source>&amp;Export histórie</source>
        <translation>&amp;Export history</translation>
    </message>
    <message>
        <location filename="../winmain.ui" line="550"/>
        <source>Export vybraných položiek z histórie</source>
        <translation>Export selected history</translation>
    </message>
    <message>
        <location filename="../winmain.ui" line="658"/>
        <source>Export ako &amp;skript</source>
        <translation>Export as &amp;script</translation>
    </message>
    <message>
        <location filename="../winmain.ui" line="667"/>
        <source>&amp;Hladaj</source>
        <translation>&amp;Find</translation>
    </message>
    <message>
        <location filename="../winmain.ui" line="670"/>
        <source>Ctrl+F</source>
        <translation>Ctrl+F</translation>
    </message>
    <message>
        <location filename="../winmain.ui" line="562"/>
        <source>&amp;Import histórie</source>
        <translation>&amp;Import from History</translation>
    </message>
    <message>
        <location filename="../winmain.ui" line="571"/>
        <source>&amp;Lokalizuj program</source>
        <translation>&amp;Localize program</translation>
    </message>
    <message>
        <location filename="../winmain.ui" line="574"/>
        <source>Ctrl+Return</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../winmain.ui" line="583"/>
        <source>O&amp;tvor prac. priečinok</source>
        <translation>&amp;Open working Directory</translation>
    </message>
    <message>
        <location filename="../winmain.ui" line="586"/>
        <source>Otvoriť pracovný priečinok</source>
        <translation>Open working Directory</translation>
    </message>
    <message>
        <location filename="../winmain.ui" line="589"/>
        <source>Ctrl+Shift+Return</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../winmain.ui" line="598"/>
        <source>&amp;Kopírovať parametre</source>
        <translation>&amp;Copy parameters</translation>
    </message>
    <message>
        <location filename="../winmain.ui" line="625"/>
        <source>Spustiť proces &amp;napozadí</source>
        <translation>Run process in &amp;background</translation>
    </message>
    <message>
        <location filename="../winmain.ui" line="601"/>
        <source>Skopírovať parametre do klipboardu</source>
        <translation>Copy parameters to clipboard</translation>
    </message>
    <message>
        <location filename="../winmain.ui" line="610"/>
        <source>&amp;Environmentálne premenné</source>
        <translation>&amp;Environment variables</translation>
    </message>
    <message>
        <location filename="../winmain.ui" line="616"/>
        <source>F4</source>
        <translation>F4</translation>
    </message>
    <message>
        <location filename="../winmain.ui" line="628"/>
        <source>Spustiť proces napozadí</source>
        <translation>Run process in background</translation>
    </message>
    <message>
        <location filename="../winmain.ui" line="640"/>
        <source>Spusti &amp;proces samostatne</source>
        <translation>Run process &amp;dettached</translation>
    </message>
    <message>
        <location filename="../winmain.ui" line="643"/>
        <source>Spusti proces samostatne</source>
        <translation>Run process dettached</translation>
    </message>
    <message>
        <location filename="../winmain.ui" line="646"/>
        <source>Ctrl+F5</source>
        <translation>CTRL+F5</translation>
    </message>
    <message>
        <location filename="../winmain.ui" line="631"/>
        <source>Shift+F5</source>
        <translation>Shift+F5</translation>
    </message>
    <message>
        <location filename="../winmain.cpp" line="438"/>
        <source>Súbor %1 neexistuje!</source>
        <translation>File %1 does not exists!</translation>
    </message>
    <message>
        <location filename="../winmain.cpp" line="444"/>
        <source>Súbor %1 nie je spustiteľný!</source>
        <translation>File %1 is not executable!</translation>
    </message>
    <message>
        <location filename="../winmain.cpp" line="495"/>
        <source>Pracovný priečinok</source>
        <translation>Working Directory</translation>
    </message>
    <message>
        <location filename="../winmain.cpp" line="525"/>
        <location filename="../winmain.cpp" line="599"/>
        <location filename="../winmain.cpp" line="602"/>
        <location filename="../winmain.cpp" line="608"/>
        <location filename="../winmain.cpp" line="612"/>
        <location filename="../winmain.cpp" line="617"/>
        <source>Chyba</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../winmain.cpp" line="510"/>
        <source>Historia</source>
        <translation>History</translation>
    </message>
    <message>
        <location filename="../winmain.cpp" line="511"/>
        <source>Uložiť</source>
        <translation>Save</translation>
    </message>
    <message>
        <location filename="../winmain.cpp" line="522"/>
        <location filename="../winmain.cpp" line="596"/>
        <source>Informácia</source>
        <translation>Information</translation>
    </message>
    <message>
        <location filename="../winmain.cpp" line="522"/>
        <source>Export ukončený</source>
        <translation>Export successful</translation>
    </message>
    <message>
        <location filename="../winmain.cpp" line="525"/>
        <source>Nie je možné otvoriť súbor %1 na zápis!</source>
        <translation>Cannot open file &quot;%1&quot; for writting!</translation>
    </message>
    <message>
        <location filename="../winmain.cpp" line="533"/>
        <source>Otvoriť</source>
        <translation>Open</translation>
    </message>
    <message>
        <location filename="../winmain.cpp" line="596"/>
        <source>Import ukončený</source>
        <translation>Import successful</translation>
    </message>
    <message>
        <location filename="../winmain.cpp" line="626"/>
        <source>Šel skript</source>
        <translation>Shell script</translation>
    </message>
    <message>
        <location filename="../winmain.cpp" line="627"/>
        <source>Všetko</source>
        <translation>All files</translation>
    </message>
    <message>
        <location filename="../winmain.cpp" line="630"/>
        <source>Aplikácia</source>
        <translation>Application</translation>
    </message>
    <message>
        <location filename="../winmain.cpp" line="522"/>
        <location filename="../winmain.cpp" line="596"/>
        <source>Počet položiek</source>
        <translation>Number of Items</translation>
    </message>
    <message>
        <location filename="../winmain.cpp" line="34"/>
        <location filename="../winmain.cpp" line="38"/>
        <source>alebo</source>
        <translation>or</translation>
    </message>
    <message>
        <location filename="../winmain.cpp" line="35"/>
        <source>Spustiť proces na pozadí pomocou SHIFT</source>
        <translation>Run process in background using SHIFT</translation>
    </message>
    <message>
        <location filename="../winmain.cpp" line="39"/>
        <source>Spustiť proces samostatne pomocou CTRL</source>
        <translation>Run process dettached using CTRL</translation>
    </message>
    <message>
        <location filename="../winmain.cpp" line="599"/>
        <source>Chyba spracovania XML na riadku</source>
        <translation>Error processing XML at row</translation>
    </message>
    <message>
        <location filename="../winmain.cpp" line="599"/>
        <source>stĺpec</source>
        <translation>column</translation>
    </message>
    <message>
        <location filename="../winmain.cpp" line="602"/>
        <source>Súbor &quot;%1&quot; nemá požadovaný formát!</source>
        <translation>File %1 has wrong format!</translation>
    </message>
    <message>
        <location filename="../winmain.cpp" line="608"/>
        <source>Nie je možné otvoriť súbor &quot;%1&quot; na čítanie!</source>
        <translation>Cannot open file &quot;%1&quot; for reading!</translation>
    </message>
    <message>
        <location filename="../winmain.cpp" line="612"/>
        <source>XSD schéma nie je validná!</source>
        <translation>Not a valid XSD scheme!</translation>
    </message>
    <message>
        <location filename="../winmain.cpp" line="617"/>
        <source>Nie je možné otvoriť súbor &quot;%1&quot;</source>
        <translation>Cannot open file &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../winmain.cpp" line="822"/>
        <source>Export ako skript</source>
        <translation>Export as script</translation>
    </message>
    <message>
        <location filename="../winmain.cpp" line="850"/>
        <source>Varovanie</source>
        <translation>Warning</translation>
    </message>
    <message>
        <location filename="../winmain.cpp" line="879"/>
        <source>Zadaj filter</source>
        <translation>Enter filter</translation>
    </message>
    <message>
        <location filename="../winmain.cpp" line="903"/>
        <source>Prevziať novú verziu</source>
        <translation>Go to download Page</translation>
    </message>
    <message>
        <location filename="../winmain.h" line="27"/>
        <source>Dostupné zmeny aplikácie</source>
        <translation>Changelog</translation>
    </message>
    <message>
        <location filename="../winmain.h" line="28"/>
        <source>&amp;Dostupná nová verzia</source>
        <translation>&amp;New Version available</translation>
    </message>
    <message>
        <location filename="../winmain.h" line="29"/>
        <source>Zmeniť pracovnú zložku</source>
        <translation>Change working Directory</translation>
    </message>
</context>
<context>
    <name>winProcess</name>
    <message>
        <location filename="../winprocess.ui" line="14"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../winprocess.ui" line="53"/>
        <source>Standard Output (Alt+1) &lt;br&gt;Standard Error (Alt+2)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../winprocess.ui" line="60"/>
        <source>Standard Output (Alt+1)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../winprocess.ui" line="63"/>
        <source>Standard Output</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../winprocess.ui" line="80"/>
        <source>Standard Error (Alt+2)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../winprocess.ui" line="83"/>
        <source>Standart Error</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../winprocess.ui" line="115"/>
        <source>Standard &amp;input</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../winprocess.ui" line="166"/>
        <source>Odošli text procesu</source>
        <translation>Send Text to Process</translation>
    </message>
    <message>
        <location filename="../winprocess.ui" line="187"/>
        <source>undefined</source>
        <translation></translation>
    </message>
    <message>
        <source>Uloziť do súboru</source>
        <translation type="vanished">Save to File</translation>
    </message>
    <message>
        <location filename="../winprocess.ui" line="234"/>
        <location filename="../winprocess.ui" line="237"/>
        <location filename="../winprocess.ui" line="242"/>
        <location filename="../winprocess.ui" line="245"/>
        <source>&amp;Uloziť do súboru</source>
        <translation>&amp;Save to File</translation>
    </message>
    <message>
        <location filename="../winprocess.cpp" line="75"/>
        <source>Čakám na štart ...</source>
        <translation>Waiting for start ...</translation>
    </message>
    <message>
        <location filename="../winprocess.cpp" line="102"/>
        <source>Pracovný priečinok %1 neexistuje</source>
        <translation>Working Directory %1 does not exists</translation>
    </message>
    <message>
        <location filename="../winprocess.cpp" line="162"/>
        <source>Otázka</source>
        <translation>Question</translation>
    </message>
    <message>
        <location filename="../winprocess.cpp" line="162"/>
        <source>Proces stále beží, naozaj ho chcete ukončiť?</source>
        <translation>Process still running, do you really want to quit?</translation>
    </message>
    <message>
        <location filename="../winprocess.cpp" line="213"/>
        <location filename="../winprocess.cpp" line="450"/>
        <location filename="../winprocess.cpp" line="516"/>
        <source>Chyba</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../winprocess.cpp" line="213"/>
        <source>Nesprávny index záložky!</source>
        <translation>Wrong tab index!</translation>
    </message>
    <message>
        <location filename="../winprocess.cpp" line="273"/>
        <source>Výber</source>
        <translation>Select</translation>
    </message>
    <message>
        <location filename="../winprocess.cpp" line="273"/>
        <source>Zvoľte znakovú sadu:</source>
        <translation>Charset:</translation>
    </message>
    <message>
        <location filename="../winprocess.cpp" line="285"/>
        <source>Nie je možné nastaviť kodek: %1</source>
        <translation>Can&apos;t use codek: %1</translation>
    </message>
    <message>
        <location filename="../winprocess.cpp" line="285"/>
        <location filename="../winprocess.cpp" line="607"/>
        <source>Warovanie</source>
        <translation>Warning</translation>
    </message>
    <message>
        <location filename="../winprocess.cpp" line="376"/>
        <source>Beží</source>
        <translation>Running</translation>
    </message>
    <message>
        <location filename="../winprocess.cpp" line="436"/>
        <source>Uložiť log súbor</source>
        <translation>Save log file</translation>
    </message>
    <message>
        <location filename="../winprocess.cpp" line="450"/>
        <source>Nepodarilo sa zapisat vystup programu do logu &quot;%1&quot;</source>
        <translation>Error writing output to log file &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../winprocess.cpp" line="454"/>
        <source>Nie je mozne otvorit subor</source>
        <translation>Cannot open file</translation>
    </message>
    <message>
        <location filename="../winprocess.cpp" line="474"/>
        <source>Uložiť HTML súbor</source>
        <translation>Save HTML file</translation>
    </message>
    <message>
        <location filename="../winprocess.cpp" line="492"/>
        <source>Program</source>
        <translatorcomment>Program</translatorcomment>
        <translation>Program</translation>
    </message>
    <message>
        <location filename="../winprocess.cpp" line="493"/>
        <source>Parametre</source>
        <translatorcomment>Parametre</translatorcomment>
        <translation>Parameters</translation>
    </message>
    <message>
        <location filename="../winprocess.cpp" line="494"/>
        <source>Start</source>
        <translation>Start</translation>
    </message>
    <message>
        <location filename="../winprocess.cpp" line="495"/>
        <source>Stop</source>
        <translation>Stop</translation>
    </message>
    <message>
        <location filename="../winprocess.cpp" line="497"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="../winprocess.cpp" line="532"/>
        <source>&amp;Znaková sada</source>
        <translation>&amp;Charset</translation>
    </message>
    <message>
        <location filename="../winprocess.cpp" line="533"/>
        <source>&amp;Font</source>
        <translation>&amp;Font</translation>
    </message>
    <message>
        <location filename="../winprocess.cpp" line="534"/>
        <source>&amp;Export do HTML</source>
        <translation>&amp;Export to HTML</translation>
    </message>
    <message>
        <location filename="../winprocess.cpp" line="535"/>
        <source>&amp;Reštart procesu</source>
        <translation>&amp;Restart process</translation>
    </message>
    <message>
        <location filename="../winprocess.cpp" line="607"/>
        <source>Nie je možné spustiť proces!</source>
        <translation>Can&apos;t run process!</translation>
    </message>
    <message>
        <location filename="../winprocess.cpp" line="496"/>
        <source>Trvanie</source>
        <translation>Duration</translation>
    </message>
    <message>
        <location filename="../winprocess.cpp" line="520"/>
        <source>HTML report</source>
        <translation>HTML report</translation>
    </message>
    <message>
        <source>Znaková sada</source>
        <translation type="vanished">Charset</translation>
    </message>
    <message>
        <source>Export do HTML</source>
        <translation type="vanished">Export to HTML</translation>
    </message>
</context>
</TS>
