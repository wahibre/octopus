/**
 ** This file is part of the Octopus project.
 ** Copyright 2023 wahibre <wahibre@gmx.com>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include "myprogramlineedit.h"
#include <QAction>
#include <QMenu>
#include <QContextMenuEvent>

MyProgramLineEdit::MyProgramLineEdit(QWidget *parent):
    QLineEdit(parent)
{

}

void MyProgramLineEdit::contextMenuEvent(QContextMenuEvent *event)
{
    QLineEdit *programEdit = this;
    QMenu* menu = createStandardContextMenu();
    QString programText = programEdit->selectionStart()!=-1 ? programEdit->selectedText() : programEdit->text();
    QAction ActFilterThis(QString("%1 \"%2\"").arg(tr("&Filter v histórii na")).arg(programText), this);

    connect(&ActFilterThis, &QAction::triggered, [=]() {
        emit ziadostNaFilter(programText);
    });

    menu->addSeparator();
    if(!programText.isEmpty())
        menu->addAction(&ActFilterThis);
    menu->exec(mapToGlobal(event->pos()));
    menu->deleteLater();

    event->accept();
}
