/**
 ** This file is part of the Octopus project.
 ** Copyright 2023 wahibre <wahibre@gmx.com>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include "historiaproxymodel.h"
#include "historiamodel.h"
#include <QBrush>


/*****************************************************************************************/
HistoriaProxyModel::HistoriaProxyModel(QObject *parent):
    QSortFilterProxyModel(parent)
{
    HistHeadersFilter[0] = QString();
    HistHeadersFilter[1] = QString();
}
/*****************************************************************************************/
void HistoriaProxyModel::nastavFilter(int section, QString filter)
{
    HistHeadersFilter[section] = filter;
    invalidateFilter();
    emit headerDataChanged(Qt::Horizontal, section, section);
}
/*****************************************************************************************/
void HistoriaProxyModel::nastavFullTextSearch(QString text, Qt::CaseSensitivity sensitivity) const
{
    qobject_cast<HistoriaModel*>(sourceModel())->setFullTextSearch(text, sensitivity);
}
/*****************************************************************************************/
QString HistoriaProxyModel::filter(int section)
{
    return HistHeadersFilter[section];
}
/*****************************************************************************************/
Prikaz HistoriaProxyModel::dajPrikaz(QModelIndex idx)
{
    return qobject_cast<HistoriaModel*>(sourceModel())->dajPrikaz(mapToSource(idx).row());
}
/*****************************************************************************************/
QModelIndexList HistoriaProxyModel::mapujNaModel(QModelIndexList list)
{
    QModelIndexList mapped;

    foreach(QModelIndex i, list)
        mapped.append(mapToSource(i));

    return mapped;
}
/*****************************************************************************************/
bool HistoriaProxyModel::existujeTextVRiadku(const QModelIndex &idx, QString text, Qt::CaseSensitivity caseSensitivity)
{
    return qobject_cast<HistoriaModel*>(sourceModel())->existujeTextVRiadku(
                mapToSource(idx),
                text,
                caseSensitivity);
}
/*****************************************************************************************/
bool HistoriaProxyModel::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const
{
    Q_UNUSED(source_parent)

    QString pole1 = sourceModel()->data(sourceModel()->index(source_row, 0)).toString();
    QString pole2 = sourceModel()->data(sourceModel()->index(source_row, 1)).toString();

    foreach(int i, QList<int>({0,1}))
    {
        if(!HistHeadersFilter[i].isEmpty() && !sourceModel()->data(sourceModel()->index(source_row, i)).toString().contains(
                    HistHeadersFilter[i], Qt::CaseInsensitive))
            return false;
    }

    return true;
}
/*****************************************************************************************/
QVariant HistoriaProxyModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(orientation == Qt::Horizontal)
    {
        if(role == Qt::DisplayRole)
        {
            QString headerLabel = sourceModel()->headerData(section, orientation, role).toString();

            if(HistHeadersFilter[section].isEmpty())
                return headerLabel;
            else
                return QString("%1 (*%2*)").arg(headerLabel).arg(HistHeadersFilter[section]);
        }

        if(role == Qt::ForegroundRole && !HistHeadersFilter[section].isEmpty())
            return  QBrush(Qt::red);
    }

    return QSortFilterProxyModel::headerData(section, orientation, role);
};
/*****************************************************************************************/
